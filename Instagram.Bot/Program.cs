﻿namespace Instagram.Bot
{
    using HtmlAgilityPack;
    using Instagram.Data;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    class Program
    {
        static void Main(string[] args)
        {
            AddUserCategory();
            Console.WriteLine("Hepsi Bitti");
            Console.ReadLine();
            var _banUserData = new BanUserData();
            
            var userList = new List<Model.BanUser>();
            var userString = new List<string>();
            int counter = 0;

            var file = "userbans.txt";
            var notFoundFile = "not_found_user.txt";

            var lines = File.ReadAllLines(file);

            foreach (var item in lines)
            {
                if (item == "explore")
                    continue;

                counter++;

                if (counter == 100)
                {
                    changeIP();
                    counter = 0;
                }
                    

                var result = HtmlGetherer(item);

                if (string.IsNullOrEmpty(result) || result == "404")
                {
                    Console.WriteLine(item+ " kullanıcı bulunamadı");
                    userString.Add(item);
                    continue;
                }

                if(result.Contains("Restricted profile"))
                {
                    Console.WriteLine("Restricted profile");
                    userString.Add(item + " Restricted profile");
                    continue;
                }

                var json = JsonConvert.DeserializeObject<InstaAccountModel>(result);
                Console.WriteLine("\t" + item);
                
            }

            File.WriteAllLines(notFoundFile, userString);
            Console.WriteLine("hepsi bitti");
            Console.ReadLine();
        }

        static void AddUserCategory()
        {
            var _popularAccountData = new PopularAccountData();
            var allUsers = _popularAccountData.GetAll();
            var list = new List<string>() { "t=pets&p=1&c=26", "t=photography&p=5&c=27", "t=politics&p=2&c=28", "t=sport-clubs&p=1&c=29", "t=tattoo&p=2&c=31", "t=travel-tourism&p=2&c=33", "t=video-games&p=1&c=34" };
            for (int l = 0; l < list.Count; l++)
            {
                var spData = list[l].Split('&');
                string page = spData[1].Replace("p=", "");
                string cat = spData[2].Replace("c=", "");
                int p = Int32.Parse(page);
                int c = Int32.Parse(cat);
                string t = spData[0].Replace("t=", "");
                for (int i = 1; i <= p; i++)
                {
                    changeIP();
                    WebClient wc = new WebClient();
                    wc.Encoding = Encoding.UTF8;
                    var url = $"https://bigsta.net/category/{t}/{i}/";
                    var latestContentHtml = wc.DownloadString(url);
                    var latestContentDoc = new HtmlDocument();
                    latestContentDoc.LoadHtml(latestContentHtml);

                    var row = latestContentDoc.DocumentNode.SelectNodes("//div[@class='some-countryTable']//div[@class='user']//a");

                    foreach (var item in row)
                    {
                        try
                        {
                            var html = item.Attributes[0].Value;
                            var usernameSplit = html.Split('/');
                            var username = usernameSplit[2];

                            var checkUser = allUsers.Where(x => x.Username == username).FirstOrDefault();
                            if (checkUser == null)
                            {
                                var instaUrl = "https://instagram.com/" + username + "?__a=1";
                                var insta = wc.DownloadString(instaUrl);

                                var json = JsonConvert.DeserializeObject<InstaAccountModel>(insta);

                                if (json.Graphql.User != null)
                                {
                                    var userModel = new Model.PopularAccount()
                                    {
                                        Followers = json.Graphql.User.Followers.Count,
                                        Followings = json.Graphql.User.Followings.Count,
                                        Fullname = json.Graphql.User.Fullname,
                                        ImagePath = json.Graphql.User.Avatar,
                                        Media = json.Graphql.User.Media.Count,
                                        Username = json.Graphql.User.Username,
                                        CategoryId = c,
                                        IsActive = true,
                                    };
                                    Console.WriteLine(userModel.Fullname + " Eklendi" );
                                    var result = _popularAccountData.Insert(userModel);
                                    allUsers.Add(userModel);
                                }
                            }
                            else
                            {
                                checkUser.CategoryId = c;
                                Console.WriteLine(checkUser.Fullname + " Kategori güncellendi");
                                _popularAccountData.Update(checkUser);
                            }
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                    }
                }
            }
        }

        static void changeIP()
        {
            new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "cmd.exe",
                    Arguments = "/C adb shell svc data disable; svc data enable;"
                }
            }.Start();
            string ipAdresi = "";
            for (int di = 1; di <= 1; di++)
            {
                try
                {
                    ipAdresi = (new System.Net.WebClient()).DownloadString("http://checkip.dyndns.org/");
                    ipAdresi = (new System.Text.RegularExpressions.Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")).Matches(ipAdresi)[0].ToString();
                    Thread.Sleep(2000);
                }
                catch
                {
                    Thread.Sleep(5000);
                    di = 0;
                    continue;
                }
            }
        }

        static string HtmlGetherer(string username)
        {
            try
            {
                WebClient wc = new WebClient();
                wc.Encoding = Encoding.UTF8;
                var instaUrl = "https://instagram.com/" + username + "?__a=1";
                return wc.DownloadString(instaUrl);
            }
            catch (WebException exc)
            {
                var response = exc.Response as HttpWebResponse;
                if (response != null)
                {
                    var statusCode = (int)response.StatusCode;
                    return statusCode.ToString();
                }

                if (exc.Response != null)
                {
                    var stream = exc.Response.GetResponseStream();
                    if (stream != null)
                    {
                        var resp = new StreamReader(stream).ReadToEnd();
                    }
                }
            }
            catch (Exception exc)
            {
            }
            return "";
        }
    }
}
