﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Instagram.Analysis
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(name: "ReportPage", url: "report-a-problem/{username}", defaults: new { controller = "Page", action = "Index", username = UrlParameter.Optional });
            routes.MapRoute(name: "ClearBans", url: "clearbans", defaults: new { controller = "Home", action = "ClearBans", username = UrlParameter.Optional });
            routes.MapRoute(name: "ReportPagexxx", url: "x/list/add", defaults: new { controller = "Home", action = "AddCategoryUser", username = UrlParameter.Optional });
            routes.MapRoute(name: "About", url: "about", defaults: new { controller = "Page", action = "About" });
            routes.MapRoute(name: "Team", url: "team", defaults: new { controller = "Page", action = "Team" });
            routes.MapRoute(name: "Company", url: "company", defaults: new { controller = "Page", action = "Company" });
            routes.MapRoute(name: "PrivacyPolicy", url: "privacy-policy", defaults: new { controller = "Page", action = "Privacy" });

            routes.MapRoute(name: "PHashtags", url: "popular-hashtags", defaults: new { controller = "List", action = "Hashtag" });
            routes.MapRoute(name: "PAccounts", url: "popular-accounts/{page}", defaults: new { controller = "List", action = "Index", page = UrlParameter.Optional });
            routes.MapRoute(name: "PCat", url: "popular-categories", defaults: new { controller = "List", action = "Category", page = UrlParameter.Optional });
            routes.MapRoute(name: "CatDetail", url: "category/{slug}/{page}", defaults: new { controller = "List", action = "Detail", page = UrlParameter.Optional });
            routes.MapRoute(name: "List", url: "list/{slug}/", defaults: new { controller = "List", action = "Index" });

            routes.MapRoute(name: "AccountTicker", url: "account/ticker/", defaults: new { controller = "Account", action = "Ticker" });
            routes.MapRoute(name: "AccountTracker", url: "account/tracker/", defaults: new { controller = "Account", action = "Tracker" });
            routes.MapRoute(name: "HashtagTracker", url: "hashtag/tracker/", defaults: new { controller = "Hashtag", action = "Tracker" });
            routes.MapRoute(name: "AccountBan", url: "{username}/ban/", defaults: new { controller = "Account", action = "Ban" });
            routes.MapRoute(name: "LocationBan", url: "loc/{id}/ban/", defaults: new { controller = "Location", action = "Ban" });
            routes.MapRoute(name: "HashtagBan", url: "tag/{hashtag}/ban/", defaults: new { controller = "Hashtag", action = "Ban" });
            routes.MapRoute(name: "MediagBan", url: "media/{code}/ban/", defaults: new { controller = "Media", action = "Ban" });

            routes.MapRoute(name: "Hashtag", url: "tag/{hashtag}/", defaults: new { controller = "Hashtag", action = "Index" });
            routes.MapRoute(name: "Media", url: "media/{code}/", defaults: new { controller = "Media", action = "Index" });
            routes.MapRoute(name: "LocationGeo", url: "geo/{id}/", defaults: new { controller = "Location", action = "Geo" });
            routes.MapRoute(name: "Location", url: "loc/{id}/", defaults: new { controller = "Location", action = "Index" });
            routes.MapRoute(name: "Search", url: "search/{param}/", defaults: new { controller = "Search", action = "Index", param = UrlParameter.Optional });
            routes.MapRoute(name: "Account", url: "{username}/", defaults: new { controller = "Account", action = "Index" });
            routes.MapRoute(name: "Followers", url: "{username}/followers/", defaults: new { controller = "Account", action = "Followers" });
            routes.MapRoute(name: "Following", url: "{username}/following/", defaults: new { controller = "Account", action = "Following" });
            routes.MapRoute(name: "Accountx", url: "{username}/{userid}/", defaults: new { controller = "Account", action = "IdRoute" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
