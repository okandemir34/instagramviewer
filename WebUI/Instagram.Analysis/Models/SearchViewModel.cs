﻿namespace Instagram.Analysis.Models
{
    public class SearchViewModel
    {
        public SearchViewModel()
        {
            IsEnableAds = true;
            Param = "";
        }

        public bool IsEnableAds { get; set; }
        public string Param { get; set; }
    }
}