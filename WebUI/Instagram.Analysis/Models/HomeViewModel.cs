﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Instagram.Analysis.Models
{
    public class HomeViewModel
    {
        public HomeViewModel()
        {
            Categories = new List<Model.Category>();
            PopularAccounts = new List<Model.PopularAccount>();
            Hashtags = new List<Model.Hashtag>();
        }

        public List<Model.Category> Categories { get; set; }
        public List<Model.PopularAccount> PopularAccounts { get; set; }
        public List<Model.Hashtag> Hashtags { get; set; }
    }
}