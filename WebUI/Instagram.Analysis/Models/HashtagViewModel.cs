﻿namespace Instagram.Analysis.Models
{
    public class HashtagViewModel
    {
        public HashtagViewModel()
        {
            IsEnableAds = true;
        }

        public bool IsEnableAds { get; set; }
        public string Hashtag { get; set; }
        public InstaHashtagModel InstagramHashtag { get; set; }
    }
}