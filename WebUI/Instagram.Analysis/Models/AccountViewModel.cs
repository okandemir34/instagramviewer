﻿namespace Instagram.Analysis.Models
{
    using System.Collections.Generic;

    public class AccountViewModel
    {
        public AccountViewModel()
        {
            IsEnableAds = true;
            Followers = new List<int>();
            Friends = new List<int>();
            Media = new List<int>();
            Date = new List<string>();
        }

        public bool IsEnableAds { get; set; }
        public string Username { get; set; }

        public List<int> Followers { get; set; }
        public List<int> Friends { get; set; }
        public List<int> Media { get; set; }
        public List<string> Date { get; set; }
    }
}