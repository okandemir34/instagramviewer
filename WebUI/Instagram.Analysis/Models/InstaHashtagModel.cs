﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Instagram.Analysis.Models
{
    public class InstaHashtagModel
    {
        [JsonProperty("graphql")]
        public HashGraphql HasGraphql { get; set; }
    }

    public partial class HashGraphql
    {
        [JsonProperty("hashtag")]
        public Hashtags Hashtag { get; set; }
    }

    public partial class Hashtags
    {
        [JsonProperty("edge_hashtag_to_media")]
        public HashtagMedia HashtagMedia { get; set; }

        [JsonProperty("edge_hashtag_to_top_posts")]
        public HashtagMedia TopMedias { get; set; }

        [JsonProperty("edge_hashtag_to_related_tags")]
        public HashtagMedia RelatedTags { get; set; }
    }

    public partial class HashtagMedia
    {
        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("edges")]
        public List<Items> Items { get; set; }
    }

    public partial class Items
    {
        [JsonProperty("node")]
        public Media Media { get; set; }
    }

    public partial class Media
    {
        [JsonProperty("__typename")]
        public string TypeName { get; set; }
        
        [JsonProperty("shortcode")]
        public string Code { get; set; }
        
        [JsonProperty("display_url")]
        public string Display { get; set; }
        
        [JsonProperty("is_video")]
        public bool IsVideo { get; set; }
    }

}