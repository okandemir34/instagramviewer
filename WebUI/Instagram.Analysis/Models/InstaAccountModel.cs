﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Instagram.Analysis.Models
{
    public class InstaAccountModel
    {
        [JsonProperty("graphql")]
        public Graphql Graphql { get; set; }
    }

    public partial class Graphql
    {
        [JsonProperty("user")]
        public User User { get; set; }
    }

    public partial class User
    {
        [JsonProperty("edge_followed_by")]
        public Counted Followers { get; set; }
        
        [JsonProperty("edge_follow")]
        public Counted Followings { get; set; }

        [JsonProperty("edge_owner_to_timeline_media")]
        public Counted Media { get; set; }

        [JsonProperty("full_name")]
        public string Fullname { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
        
        [JsonProperty("profile_pic_url_hd")]
        public string Avatar { get; set; }
        
        [JsonProperty("biography")]
        public string Biography { get; set; }
    }
    
    public partial class Counted
    {
        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("edges")]
        public List<Edges> Edges { get; set; }
    }

    public partial class Edges
    {
        [JsonProperty("node")]
        public Node Node { get; set; }
    }

    public partial class Node
    {
        [JsonProperty("__typename")]
        public string MediaType { get; set; }
        
        [JsonProperty("edge_media_to_caption")]
        public MediaEdges Caption { get; set; }

        [JsonProperty("shortcode")]
        public string ImageShort { get; set; }
        
        [JsonProperty("display_url")]
        public string DisplayUrl { get; set; }

        [JsonProperty("location")]
        public Location Location { get; set; }

        [JsonProperty("edge_media_to_comment")]
        public Counted CommentCount { get; set; }
        
        [JsonProperty("edge_liked_by")]
        public Counted LikeCount { get; set; }
        
        [JsonProperty("is_video")]
        public bool IsVideo { get; set; }
    }

    public partial class Location
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }
    }

    public partial class MediaEdges
    {
        [JsonProperty("edges")]
        public EdgeMediaToCaptionEdge[] Edges { get; set; }
    }

    public partial class EdgeMediaToCaptionEdge
    {
        [JsonProperty("node")]
        public FluffyNode Node { get; set; }
    }

    public partial class FluffyNode
    {
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}