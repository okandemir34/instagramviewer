﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Instagram.Analysis.Models
{
    public class ReportModel
    {
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string MailAddress { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public int Captha1 { get; set; }
        public int Captha2 { get; set; }
    }
}