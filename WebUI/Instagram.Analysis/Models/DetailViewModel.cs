﻿namespace Instagram.Analysis.Models
{
    using Instagram.Model;
    using System.Collections.Generic;

    public class DetailViewModel
    {
        public DetailViewModel()
        {
            PopularAccounts = new List<PopularAccount>();
        }

        public List<PopularAccount> PopularAccounts { get; set; }
        public Category Category { get; set; }
        public int Page { get; set; }
    }
}