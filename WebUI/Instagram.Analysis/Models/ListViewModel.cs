﻿namespace Instagram.Analysis.Models
{
    using Instagram.Model;
    using System.Collections.Generic;

    public class ListViewModel
    {
        public ListViewModel()
        {
            PopularAccounts = new List<PopularAccount>();
        }

        public List<PopularAccount> PopularAccounts { get; set; }
        public int Page { get; set; }
    }
}