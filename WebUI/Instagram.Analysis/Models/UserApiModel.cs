﻿using Newtonsoft.Json;

namespace Instagram.Analysis.Models
{
    public class UserApiModel
    {
        [JsonProperty("graphql")]
        public Graphql Graphql { get; set; }

    }
}