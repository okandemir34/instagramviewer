﻿using Newtonsoft.Json;

namespace Instagram.Analysis.Models
{
    public class UserTxtFileModel
    {
        public string tx { get; set; }
        public string us { get; set; }
    }
}