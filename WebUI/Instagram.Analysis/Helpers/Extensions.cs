﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Instagram.Analysis.Helpers
{
    public static class Extensions
    {
        /// <summary>
        /// Datetime to Time ago string
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToTimeAgo(this DateTime dt)
        {
            TimeSpan span = DateTime.Now - dt;
            if (span.Days > 365)
            {
                int years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("{0} years", years);
            }
            if (span.Days >= 7)
                return String.Format("{0} weeks", (int)(span.Days / 7));
            if (span.Days < 7 && span.Days > 0)
                return String.Format("{0} days", span.Days);
            if (span.Hours > 0)
                return String.Format("{0} hours", span.Hours);
            if (span.Minutes > 0)
                return String.Format("{0} minutes", span.Minutes);
            if (span.Seconds > 5)
                return String.Format("{0} seconds", span.Seconds);
            if (span.Seconds <= 5)
                return "0s";
            return string.Empty;
        }

        public static string ToNiceNumber(this int num)
        {
            // Ensure number has max 3 significant digits (no rounding up can happen)
            int i = (int)Math.Pow(10, (int)Math.Max(0, Math.Log10(num) - 2));
            num = num / i * i;

            if (num >= 1000000000)
                return (num / 1000000000D).ToString("0.##") + "B";
            if (num >= 1000000)
                return (num / 1000000D).ToString("0.##") + "M";
            if (num >= 1000)
                return (num / 1000D).ToString("0.##") + "K";

            return num.ToString("#,0");
        }

        public static string ToTrim(this string value, int length)
        {
            if (value.Length < length)
                return value;

            return value.Substring(0, length - 1);
        }

        public static string ToSlug(this string phrase)
        {
            if (phrase == null)
                phrase = "";

            //clear punctuation
            string str = phrase.ClearPunctuation().ToLowerInvariant();

            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();

            // cut and trim 
            str = Regex.Replace(str, @"\s", "-"); //hyphens  

            str = str.Replace("ı", "i")
                    .Replace("ğ", "g")
                    .Replace("ö", "o")
                    .Replace("ü", "u")
                    .Replace("ş", "s")
                    .Replace("$", "s")
                    .Replace("ş", "s");

            return str;
        }

        private static string ClearPunctuation(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return value;

            var list = new List<Char>();
            foreach (char c in value)
            {
                if (Char.IsPunctuation(c))
                    continue;

                list.Add(c);
            }

            value = string.Concat(list.ToArray());

            return value;
        }

        public static List<string> ToHashtags(this string text)
        {
            var list = new List<string>();
            if (string.IsNullOrEmpty(text))
                return list;

            var regex = new Regex(@"[#](\W*[a-zA-Z0-9_ğüşıöçĞÜŞİÖÇ]\w*)");
            var matches = regex.Matches(text);
            var nonAllowedChars = new List<string>() { "&", "@", "\\", "\n", "(", ")", "<", ">", "!", "'", " " };

            foreach (Match m in matches)
            {
                if (m.Value.Length <= 5)
                    continue;

                foreach (var str in nonAllowedChars)
                {
                    if (m.Value.Contains(str))
                        continue;
                }

                list.Add(m.Value.Replace("#", "").ToLower());
            }

            return list;
        }

        public static List<string> ToMentions(this string text)
        {
            var list = new List<string>();
            if (string.IsNullOrEmpty(text))
                return list;

            var regex = new Regex(@"[@](\W*[a-zA-Z0-9_ğüşıöçĞÜŞİÖÇ]\w*)");
            var matches = regex.Matches(text);
            var nonAllowedChars = new List<string>() { "&", "#", "\\", "\n", "(", ")", "<", ">", "!", "'", " " };

            foreach (Match m in matches)
            {
                if (m.Value.Length <= 4)
                    continue;

                foreach (var str in nonAllowedChars)
                {
                    if (m.Value.Contains(str))
                        continue;
                }

                list.Add(m.Value.Replace("@", "").ToLower());
            }

            return list;
        }

        public static string ToFormattedInt(this int value)
        {
            return string.Format("{0:n0}", value);
        }

        public static string ToFormattedDecimal(this decimal value)
        {
            return value.ToString("#,##0.00");
        }

        public static string StripHtml(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return "";

            return Regex.Replace(value, "<.*?>", String.Empty);
        }
    }
}