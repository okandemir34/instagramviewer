﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Instagram.Analysis.Helpers
{
    public class DomainHelper
    {
        public static void EnsureSslAndDomain(HttpContext context, bool redirectNonWww, bool redirectSsl)
        {
            if (context == null)
                return;

            var redirect = false;
            var uri = context.Request.Url;
            var scheme = uri.GetComponents(UriComponents.Scheme, UriFormat.Unescaped);
            var host = uri.GetComponents(UriComponents.Host, UriFormat.Unescaped);
            var pathAndQuery = uri.GetComponents(UriComponents.PathAndQuery, UriFormat.Unescaped);

            if (redirectSsl && !scheme.Equals("https"))
            {
                scheme = "https";
                redirect = true;
            }

            if (!redirectSsl && scheme.Equals("https"))
            {
                scheme = "http";
                redirect = true;
            }

            if (redirectNonWww && host.StartsWith("www", StringComparison.OrdinalIgnoreCase))
            {
                host = host.Replace("www.", "");
                redirect = true;
            }

            if (redirect)
            {
                context.Response.Status = "301 Moved Permanently";
                context.Response.StatusCode = 301;
                context.Response.AddHeader("Location", scheme + "://" + host + pathAndQuery);
            }
        }
    }
}