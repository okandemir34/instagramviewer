﻿namespace Instagram.Analysis.Helpers
{
    public static class SeoHelper
    {
        public static string UserAnchorTitle(string username)
        {
            return $"{username}'s Instagram Username and {username}'s Instagram Photos and Videos";
        }

        public static string SnapcodeImgTitle(string fullName)
        {
            return $"{fullName}'s Snapchat Code";
        }

        public static string HashtagAnchorTitle(string tag)
        {
            return $"Browse {tag} Snapchats, Find {tag} Snapchat Usernames";
        }

        public static string HashtagAnchorTitlePage(string tag, int page)
        {
            return $"Browse {tag} Snapchats Page {page}, Find {tag} Snapchat Usernames Page {page}";
        }
    }
}