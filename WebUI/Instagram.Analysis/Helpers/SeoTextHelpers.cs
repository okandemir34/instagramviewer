﻿namespace Instagram.Analysis.Helpers
{
    public static class SeoTextHelpers
    {
        public static string AccountTitle(this string username, string fullname)
        {
            return string.IsNullOrEmpty(fullname)
                ? $"{username}'s Pinterest Account, Images and Ideas"
                : $"{fullname} ({username})'s Pinterest Account, Images and Ideas";
        }

        public static string AccountTitleFromUsername(this string username, string fullname)
        {
            return string.IsNullOrEmpty(fullname)
                ? $"{username}'s Pinterest Account, Images and Ideas"
                : $"{fullname} ({username})'s Pinterest Account, Images and Ideas";
        }

        public static string AccountSubTitle(this string username, string fullname)
        {
            return $"List Pinterest {username} Images, Videos and Ideas shared on Pinterest Account. Also find {fullname}'s Pinterest Account Statistics, Engagement Rates and Analytics.";
        }

        public static string AccountDescription(this string username, string fullname)
        {
            return $"List Pinterest {username} Images, Videos and Ideas shared on Pinterest Account. Also find {fullname}'s Pinterest Account Statistics, Engagement Rates and Analytics.";
        }

        public static string AccountAvatarTitle(this string username, string fullname)
        {
            return string.IsNullOrEmpty(fullname)
                ? $"{username}'s Pinterest Account Avatar"
                : $"{fullname}'s Pinterest Account Avatar";
        }

        public static string AccountAvatarTitleFromUsername(this string username, string fullName)
        {
            return string.IsNullOrEmpty(fullName)
                ? $"{username}'s Pinterest Account Avatar"
                : $"{fullName}'s Pinterest Account Avatar";
        }

        public static string HashtagTitle(this string hashtag)
        {
            return $"Browse {hashtag} Images and Ideas on Pinterest";
        }

        public static string MentionTitle(this string username)
        {
            return $"Browse {username} Images and Ideas on Pinterest";
        }

        public static string HashtagDescription(this string hashtag)
        {
            return $"Browse {hashtag} Images, Videos and Ideas recently shared on Pinterest. Also find suggestions for pinterest hashtag #{hashtag}.";
        }

        public static string MediaTitle(this string caption, string username)
        {
            if (!string.IsNullOrEmpty(caption))
            {
                return caption.ToTrim(150);
            }

            return $"{username}'s Pinterest Image {caption}";
        }

        public static string MediaDescription(this string caption, string username)
        {
            if (!string.IsNullOrEmpty(caption))
            {
                return caption.ToTrim(150);
            }

            return $"{username}'s Pinterest Image {caption}";
        }
    }
}