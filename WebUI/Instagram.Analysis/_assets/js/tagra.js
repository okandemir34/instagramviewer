﻿const main = (function () {

    const services = {
        "api": {
            "base": "https://www.instagram.com/",
            "loader": $(".loader"),
        }
    };

    const ajaxHelper = function (settings, callback) {
        var returnOBJ = {};
        $.ajax(settings)
            .done(function (e) {
                returnOBJ.error = false;
                returnOBJ.code = "";
                returnOBJ.data = e;
                callback(e);
            })
            .fail(function (e) {
                returnOBJ.error = true;
                returnOBJ.code = "1";
                returnOBJ.data = "";
                callback(e);
            });
    };

    return {
        ajaxHelper: ajaxHelper,
        services: services,
    }
}());

const instagram = (function () {

    const variables = {
        "element": {
            "api_base": main.services.api.base,
            "loader": $(".loader"),
            "jumbotron_subtitle": 'Reach analytical data of ##fullname## - @##username## Instagram profile. Browse Instagram photos and videos of @##username##',
            "account_title": '##fullname##\'s Instagram and ##username##\'s Instagram Photos and Videos',
            "account_description": 'You can find ##fullname##\'s Instagram and ##fullname##\'s Instagram Medias. This page contains also other social media platform links of Instagram ##username##.',
            "account_info":     '<div class="userInstagramAvatar">'+
                                    '<img src="##avatar##"'+
                                         'alt="##fullname##\'s Videos in ##username## Tiktok Account"'+
                                         'title="##fullname##\'s Videos in ##username## Tiktok Account"'+
                                         'width="120" height="120" />'+
                                '</div>'+
                                '<p>##fullname##</p>'+
                                '<p>@##username##</p>'+
                                '<p>##biography##</p>',
            "account_stat": '<div class="instagramUserStats">'+
                                '<i class="##icon##"></i>'+
                                '<p>##name##</p>'+
                                '<p>##count##</p>'+
                            '</div>',
            "action_stat": '<div class="siteBox-3 userLastStats">'+
                                '<i class="icon-user"></i>'+
                                '<p>Last Metions</p>'+
                                '<ul>'+
                                '</ul>'+
                            '</div>',
            "profile_posts" : '<div class="siteMediaListBox siteBox-3">'+
                                '<div class="instagramMediasBox">'+
                                    '<div class="instagramMedia">'+
                                        '<a href="/media/##mediaHref##/" title="caption"> <img class="zoom" src="##mediapath##" alt="##caption##" title="##caption##"></a>'+
                                    '</div>'+
                                    '<div class="instagramMediaDescription">'+
                                       '##captionWithTags##'+
                                    '</div>'+
                                    '##locations##'+
                                    '<div class="instagramMediaStats">'+
                                        '<p> <i class="icon-heart"></i> ##likeCount##</p>'+
                                        '<p><i class="icon-bubbles"></i> ##commentCount##</p>'+
                                        '<p><i class="icon-forward"></i>Share</p>'+
                                    '</div>'+
                                '</div>'+
                            '</div>',
            "hashtag_posts" : '<div class="siteMediaListBox siteBox-3">'+
                                '<div class="instagramMediasBox">'+
                                    '<div class="instagramMedia">'+
                                        '<a href="/media/##mediaHref##/" title="caption"> <img class="zoom" src="##mediapath##" alt="##caption##" title="##caption##"></a>'+
                                    '</div>'+
                                    '<div class="instagramMediaDescription">'+
                                       '##captionWithTags##'+
                                    '</div>'+
                                    '<div class="instagramMediaStats">'+
                                        '<p> <i class="icon-heart"></i> ##likeCount##</p>'+
                                        '<p><i class="icon-bubbles"></i> ##commentCount##</p>'+
                                        '<p><i class="icon-forward"></i>Share</p>'+
                                    '</div>'+
                                '</div>'+
                            '</div>',
            "search_hashtag": '<div class="searchTagBox siteBox-1">' +
                '<a href="##hashtagHref##/" title="##hashtagTitle##">' +
                '<p class="hashtagName">##tag##</p>' +
                '<p class="hashtagCount">##count## Posts</p>' +
                '</a>' +
                '</div>',
            "search_user": '<div class="siteBox-3">' +
                '<div class="siteSearchUserBox">' +
                '<a href="##accountHref##" title="##accountTitle##">' +
                '<img src="##avatar##" alt="##accountTitle##" title="##accountTitle##">' +
                '<p class="fullname">##fullname##</p>' +
                '<p class="username">@##username##</p>' +
                '</a>' +
                '</div>' +
                '</div>',
            "doc_title": '<h1>Analytical data of ##fullname## (@##username##) Instagram profile</h1>',
            "totalLike": 0,
            "totalComment": 0,
            "totalMention": [],
            "totalHastags": [],
            "lastShare": "",
            "totalLocations": [],
            "totalMedia": 0,
            ajax_settings: {
                type: "GET"
            },
        }
    };

    function bindEvents() {
    }

    function initProfile(username, popularTag) {
        variables.element.ajax_settings.url = variables.element.api_base + username + "/?__a=1";
        main.ajaxHelper(variables.element.ajax_settings, function (e) {
            if (e.status === 404) {
                $("#docTitle").append("<p>@" + username + " 's Instagram Account does not exist!</p>");
                $("#docTitle").append('<p class="private-small">But you may be interested in this ' + popularTag + ' Photos and Videos</p>');
                return initHashtag(tag);
            }

            if (e.graphql == undefined) {
                var title = $(e).filter('title');
                if (title.length > 0) {
                    $("#docTitle").append("<p>" + title.text() + "</p>");
                    $("#docTitle").append('<p class="private-small">But you may be interested in this ' + popularTag + ' Photos and Videos</p>');
                    return initHashtag(tag);
                }
            }

            var user = e.graphql.user;

            if (user.is_private) {
                $("#docTitle").append('<p>@' + username + '\'s Instagram Account Is Private</p> <p class="private-small">But you may be interested in this ' + popularTag + ' Photos and Videos</p>');
                $("#docTitle").append('');
                $(".instagramInfos").remove();
                $(".instagramUserAnalytics").remove();
                $(".userLastStatsBox").remove();
                $("h1").html('@' + username + '\'s Instagram Account Is Private');
                $("h2").html('But you may be interested in this ' + popularTag + ' Photos and Videos');
                $(".instagramStats").remove();
                initHashtag(popularTag);
                return;
            }

            $(".instagramInfos").append(variables.element.account_info
                .replace(/##avatar##/g, user.profile_pic_url_hd)
                .replace(/##username##/g, user.username)
                .replace(/##fullname##/g, user.full_name)
                .replace(/##biography##/g, user.biography)
                .replace(/##externalurl##/g, user.external_url)
            );

            $("#docTitle").append(variables.element.doc_title
                .replace(/##username##/g, user.username)
                .replace(/##fullname##/g, user.full_name)
            );

            variables.element.totalMedia = user.edge_owner_to_timeline_media.count;

            if (user.full_name === "" || user.full_name === undefined)
                user.full_name = user.username;

            $("h1").html(user.full_name + "'s Followers/Fans and Friends in @" + user.username + " Instagram Account");
            $("h2").html("Find All Instagram Followers/Fans and Friends of " + user.full_name + " in @" + user.username + " Instagram Account.");
            $(".username").html(user.full_name);

            
            document.title = variables.element.account_title
                .replace(/##username##/g, user.username)
                .replace(/##fullname##/g, user.full_name);

            $('meta[name="description"]').attr("content"
                , variables.element.account_description
                    .replace(/##username##/g, user.username)
                    .replace(/##fullname##/g, user.full_name)
            );

            if (user.edge_owner_to_timeline_media.count <= 4) {
                initHashtag(popularTag);
                $("#docTitle").remove();
                $(".centered").append('But you may be interested in this ' + popularTag + ' Photos and Videos')
                return;
            }

            for (i in user.edge_owner_to_timeline_media.edges) {

                var media = user.edge_owner_to_timeline_media.edges[i].node;

                var caption = "";
                if (media.edge_media_to_caption.edges.length > 0) {
                    caption = media.edge_media_to_caption.edges[0].node.text
                        .replace(/"/g, '')
                        .replace(/</g, '');
                }

                var hashtags = findHashtags(caption);
                var captionWithTagges = caption;
                var usedHashtags = [];
                if (hashtags) {
                    for (j in hashtags) {
                        if (j > 3) {
                            var addlist = hashtags[j].replace("#", "").toLocaleLowerCase();

                            if (usedHashtags.indexOf(addlist) >= 0)
                                continue;

                            if (variables.element.totalHastags.indexOf(addlist) >= 0)
                                continue;

                            variables.element.totalHastags.push(addlist);
                            continue;
                        }

                        

                        var tag = hashtags[j].replace("#", "").toLocaleLowerCase();
                        if (usedHashtags.indexOf(tag) >= 0)
                            continue;

                        variables.element.totalHastags.push(tag);

                        var url = '/tag/' + tag + '/';
                        var titleProfile = '#' + tag + ' Instagram Hashtags Feed';

                        captionWithTagges = captionWithTagges.replace(' #' + tag, ' <a href="' + url + '" title="' + titleProfile + '" >#' + tag + '</a>');

                        usedHashtags.push(tag);
                    }
                }

                var mentions = findMentions(caption);
                var captionWithMentions = captionWithTagges;
                if (mentions) {
                    for (k in mentions) {
                        if (k > 3) {
                            var addlistm = mentions[k].replace("@", "").toLocaleLowerCase();
                            variables.element.totalMention.push(addlistm);
                            continue;
                        }

                        var mention = mentions[k].replace("@", "").toLocaleLowerCase();
                        console.log(mention);

                        variables.element.totalMention.push(mention);

                        var urlMention = '/' + mention + '/';
                        var titleMention = mention + ' Photos and Videos in Instagram Account Download Medias';

                        captionWithMentions = captionWithMentions.replace(' @' + mention, ' <a href="' + urlMention + '" title="' + titleMention + '" >@' + mention + '</a>');

                        if (k > 3) { break; }
                    }
                }

                var location = findLocations(media.location);
                variables.element.indexer = i;

                variables.element.totalLike = variables.element.totalLike + media.edge_liked_by.count;
                variables.element.totalComment = variables.element.totalComment + media.edge_media_to_comment.count;

                $(".instagramUserMediaList").append(variables.element.profile_posts
                    .replace(/##mediaid##/g, media.id)
                    .replace(/##username##/g, user.username)
                    .replace(/##caption##/g, caption)
                    .replace(/##captionWithTags##/g, captionWithMentions)
                    .replace(/##mediapath##/g, media.display_url)
                    .replace(/##likeCount##/g, intToString(media.edge_liked_by.count))
                    .replace(/##commentCount##/g, intToString(media.edge_media_to_comment.count))
                    .replace(/##locations##/g, location)
                    .replace(/##indexer##/g, i)
                    .replace(/##mediawidth##/g, media.dimensions.width)
                    .replace(/##mediaheight##/g, media.dimensions.height)
                    .replace(/##fullname##/g, user.full_name)
                    .replace(/##avatar##/g, user.profile_pic_url_hd)
                    .replace(/##mediaHref##/g, media.shortcode)
                );
            }

            for (var i = 0; i < 5; i++) {
                var icon = "";
                var name = "";
                var count = "";

                if (i === 0) { icon = "icon-file-picture"; name = "Medias"; count = formatNumber(variables.element.totalMedia); }
                else if (i === 1) { icon = "icon-user"; name = "Friends"; count = formatNumber(user.edge_followed_by.count); }
                else if (i === 2) { icon = "icon-users"; name = "Followers"; count = formatNumber(user.edge_follow.count); }
                else if (i === 3) { icon = "icon-heart"; name = "Total Likes"; count = formatNumber(variables.element.totalLike); }
                else if (i === 4) { icon = "icon-bubbles2"; name = "Total Comments"; count = formatNumber(variables.element.totalComment); }

                $(".instagramUserAnalytics").append(variables.element.account_stat
                    .replace(/##name##/g, name)
                    .replace(/##icon##/g, icon)
                    .replace(/##count##/g, count)
                );
            }

            var mentionCount = 0;
            for (y in variables.element.totalMention) {
                mentionCount = mentionCount + 1;
                if (mentionCount === 10)
                    break;

                var it = variables.element.totalMention[y];
                if (it !== null) {
                    $(".mentions > ul").append("<li>"+it+"</li>");
                }
            }

            var hashCount = 0;
            for (z in variables.element.totalHastags) {
                hashCount = hashCount + 1;
                if (hashCount === 10)
                    break;

                var ht = variables.element.totalHastags[z];

                

                if (ht !== null) {
                    $(".hashtags > ul").append("<li>" + ht + "</li>");
                }
            }

            var locCount = 0;

            if (variables.element.totalLocations.length === 0) {
                $(".locations > ul").append("<li>None</li>");
            }

            for (x in variables.element.totalLocations) {
                locCount = locCount + 1;
                if (locCount === 10)
                    break;

                var lc = variables.element.totalLocations[x];
                if (lc !== null) {
                    $(".locations > ul").append("<li>" + lc + "</li>");
                }
            }

            userTicker(user.username, user.edge_followed_by.count, user.edge_follow.count, user.edge_owner_to_timeline_media.count, user.full_name, user.id);
            variables.element.loader.fadeOut(100);
        });
    }

    function initHashtag(tag) {
        variables.element.ajax_settings.url = variables.element.api_base + "explore/tags/" + tag + "/?__a=1&hl=en";
        main.ajaxHelper(variables.element.ajax_settings, function (e) {
            console.log(e);
            if (e.status === 404) {
                return initHashtag('nature');
            }

            $(".pageItemCount > p > span").html(formatNumber(e.graphql.hashtag.edge_hashtag_to_media.count) + " Total Posts");

            var medias = e.graphql.hashtag.edge_hashtag_to_media.edges;
            if (medias.length == 0) {
                medias = e.graphql.hashtag.edge_hashtag_to_top_posts.edges;
            }

            for (i in medias) {

                var media = medias[i].node;
                var caption = "";
                if (media.edge_media_to_caption.edges.length > 0) {
                    caption = media.edge_media_to_caption.edges[0].node.text
                        .replace(/"/g, '')
                        .replace(/</g, '');
                }

                var hashtags = findHashtags(caption);
                var captionWithTagges = caption;
                var usedHashtags = [];
                usedHashtags.push(tag);
                if (hashtags) {
                    for (j in hashtags) {
                        var tagInCaption = hashtags[j].replace("#", "").toLocaleLowerCase();
                        if (usedHashtags.indexOf(tagInCaption) >= 0 || tag == tagInCaption)
                            continue;

                        var url = '/tag/' + tagInCaption + '/';
                        var title = '#' + tag + ' Instagram Feed Photos and Videos in Web Viewer';

                        captionWithTagges = captionWithTagges.replace(' #' + tagInCaption, ' <a href="' + url + '" title="' + title + '" >#' + tagInCaption + '</a>');

                        usedHashtags.push(tagInCaption);

                        if (j > 3) { break; }
                    }
                }

                var mentions = findMentions(caption);
                var captionWithMentions = captionWithTagges;
                if (mentions) {
                    for (k in mentions) {
                        var mention = mentions[k].replace("@", "").toLocaleLowerCase();

                        var urlMention = '/' + mention + '/';
                        var titleMention = mention + ' Photos in Instagram Account';

                        captionWithMentions = captionWithMentions.replace(' @' + mention, ' <a href="' + urlMention + '" title="' + titleMention + '" >@' + mention + '</a>');

                        if (k > 3) { break; }
                    }
                }

                $(".instagramUserMediaList").append(variables.element.hashtag_posts
                    .replace(/##mediaid##/g, media.id)
                    .replace(/##caption##/g, caption)
                    .replace(/##captionWithTags##/g, captionWithMentions)
                    .replace(/##mediapath##/g, media.display_url)
                    .replace(/##likeCount##/g, intToString(media.edge_liked_by.count))
                    .replace(/##commentCount##/g, intToString(media.edge_media_to_comment.count))
                    .replace(/##mediawidth##/g, media.dimensions.width)
                    .replace(/##mediaheight##/g, media.dimensions.height)
                    .replace(/##mediaHref##/g, media.shortcode)
                );
            }

            variables.element.loader.fadeOut(100);
        });
    }

    function initResults(query) {
        variables.element.ajax_settings.url = variables.element.api_base + "web/search/topsearch/?query=" + query;
        main.ajaxHelper(variables.element.ajax_settings, function (e) {
            var users = e.users;
            var hashtags = e.hashtags;
            for (k in users) {
                var item = users[k].user;
                var isPrivate = item.is_private;
                if (!isPrivate) {
                    $(".instagramUserList").append(variables.element.search_user
                        .replace(/##username##/g, item.username)
                        .replace(/##fullname##/g, item.full_name)
                        .replace(/##accountTitle##/g, item.full_name + "'s Photos in " + item.username + " Instagram medias")
                        .replace(/##accountHref##/g, "/" + item.username + "/")
                        .replace(/##avatar##/g, item.profile_pic_url));
                }
            }

            for (k in hashtags) {
                var tag = hashtags[k].hashtag;

                $(".instagramHashtagList").append(variables.element.search_hashtag
                    .replace(/##tag##/g, tag.name)
                    .replace(/##hashtagHref##/g, "/tag/" + tag.name + "")
                    .replace(/##hashtagTitle##/g, "#" + tag.name + " Instagram Feed")
                    .replace(/##count##/g, tag.media_count));
            }

            variables.element.loader.fadeOut(100);
        });
    }

    function initMedia(code, popularTag) {
        console.log(code);
        if (code.length > 12)
            code = getShortcodeFromTag(code);

        variables.element.ajax_settings.url = variables.element.api_base + "p/" + code + "/?__a=1";
        main.ajaxHelper(variables.element.ajax_settings.url, function (e) {

            main.services.api.loader.fadeIn(100);

            if (e.status === 404) {
                $(".siteContainer:first-child").remove();
                $("#docTitle").append('<p>@' + username + '\'s Instagram Account Is Private</p> <p class="private-small">But you may be interested in this ' + popularTag + ' Photos and Videos</p>');
                $("#docTitle").append('');
                initHashtag(popularTag);
                return;
            }
            
            var mediaPath = e.graphql.shortcode_media.display_url;
            var width = e.graphql.shortcode_media.dimensions.width;
            var height = e.graphql.shortcode_media.dimensions.height;
            var likeCount = formatNumber(e.graphql.shortcode_media.edge_media_preview_like.count);
            var commentCount = formatNumber(e.graphql.shortcode_media.edge_media_to_parent_comment.count);
            var comments = "No Comment Yet";
            var caption = "";
            if (e.graphql.shortcode_media.edge_media_to_caption.edges.length > 0) {
                caption = e.graphql.shortcode_media.edge_media_to_caption.edges[0].node.text;
            }

            var user = e.graphql.shortcode_media.owner;
            $(".instagramUserMediaList").append(initProfile(user.username));
            
            $(".tiktokVideoUserInfos").append('<img src="' + user.profile_pic_url + '" />');
            if (!user.is_private) {
                $(".tiktokVideoUserInfos").append('<a href="/' + user.username + '/" title="' + user.full_name + ' instagram photos and videos in ' + user.username + '" > <p>' + user.full_name + '</p><p>' + user.username + '</p></a>');
            }

            if (e.graphql.shortcode_media.location !== null) {
                $(".tiktokVideoUserInfos").append('<a href="/loc/' + e.graphql.shortcode_media.location.id + '" title="' + e.graphql.shortcode_media.location.name + ' places instagram photos and videos">' + e.graphql.shortcode_media.location.name + '</a>');
            }

            if (commentCount > 0) {
                comments = e.graphql.shortcode_media.edge_media_to_parent_comment.edges;
                var html = "";
                for (i in comments) {
                    var item = comments[i].node;
                    var comment = item.text;
                    var image = item.owner.profile_pic_url;
                    var username = item.owner.username;
                    html += '<div class="tiktokCommentUser">'+
                                '<a href="/'+username+'/" title="' + username + ' instagram photos and videos in ' + username + '">'+
                                    '<img src="'+image+'" alt="'+username+' instagram photos and videos and download medias" title="'+username+' instagram photos and videos and download medias">'+
                                    '<p>'+username+'</p>'+
                                    '<p>@'+username+'</p>'+
                                '</a>'+
                                '<div class="tiktokComment">'+
                                    comment+
                                '</div>'+
                            '</div>';
                }
                comments = html;
            }

            $(".tiktokUserVideoComment").append(comments);
            $('meta[name="description"]').attr("content", e.graphql.shortcode_media.owner.username + " Medias : " + caption);
            document.title = caption;
            $("h1").html(caption);

            $(".tiktokVideoStatsAndDesc").html('<p><i class="icon-heart"></i> '+likeCount+'</p>'+
                                                '<p><i class="icon-bubbles"></i> '+commentCount+'</p>'+
                                                '<div class="description">'+caption+'</div>');

            if (e.graphql.shortcode_media.__typename === "GraphSidecar") {
                var edges = e.graphql.shortcode_media.edge_sidecar_to_children.edges;
                for (x in edges) {
                    var m = edges[x].node;
                    console.log(m);
                    $(".tiktokVideoPlayer").append('<img src="' + m.display_url + '" alt="' + caption + '" title="' + caption + '" />');
                }
            }
            else if (e.graphql.shortcode_media.__typename === "GraphVideo") {
                 $(".tiktokVideoPlayer").append('<video poster="' + mediaPath + '" src="' + e.graphql.shortcode_media.video_url+'" controls>');
            }
            else{
                $(".tiktokVideoPlayer").append('<img src="' + e.graphql.shortcode_media.display_url + '" alt="' + caption + '" title="' + caption + '" />');
            }

            variables.element.loader.fadeOut(100);
        });
    }

    function initLocation(id, popularTag) {
        console.log(id);
        var url = main.services.api.base + 'graphql/query/?query_hash=1b84447a4d8b6d6d0426fefb34514485&variables={"id":"' + id + '","first":18,"after":"2207303712618232376"}';
        variables.element.ajax_settings.url = url;
        main.ajaxHelper(variables.element.ajax_settings, function (e) {
            var loc = e.data.location;
            if (loc == null || loc == "undefined") {
                initHashtag(popularTag);
            }
            document.title = loc.name + " Instagram medias +[" + intToString(loc.edge_location_to_media.count) + "] photos and videos";
            $('meta[name="description"]').attr("content",loc.name + " Instagram medias +[" + intToString(loc.edge_location_to_media.count) + "] photos and videos, analysis instagram medias and account");
            $(".locationTitle").html(loc.name + "(Total " + intToString(loc.edge_location_to_media.count) + " Medias)");
            $("h1").html("Browse Instagram " + loc.name + " Medias");
            $("h2").html("Browse Instagram " + loc.name + " location, Browse " + loc.name + " photos, videos and " + loc.name + " medias");
            $(".pageItemCount").html(formatNumber(loc.edge_location_to_media.count) +" Medias");
            var topMedias = e.data.location.edge_location_to_top_posts.edges;
            var recentMedias = e.data.location.edge_location_to_media.edges;

            for (k in recentMedias) {
                item = topMedias[k].node;

                caption = "";
                if (item.edge_media_to_caption.edges.length > 0) {
                    caption = item.edge_media_to_caption.edges[0].node.text
                        .replace(/"/g, '')
                        .replace(/</g, '');
                }

                hashtags = findHashtags(caption);
                captionWithTagges = caption;
                usedHashtags = [];
                if (hashtags) {
                    for (j in hashtags) {
                        tagInCaption = hashtags[j].replace("#", "").toLocaleLowerCase();
                        if (usedHashtags.indexOf(tagInCaption) >= 0)
                            continue;

                        url = '/tag/' + tagInCaption + '/';
                        title = '#' + tagInCaption + ' Photos, Videos, Stories and Highlight Instagram Feed';

                        captionWithTagges = captionWithTagges.replace(' #' + tagInCaption, ' <a href="' + url + '" title="' + title + '" >#' + tagInCaption + '</a>');

                        usedHashtags.push(tagInCaption);

                        if (j > 3) { break; }
                    }
                }

                mentions = findMentions(caption);
                captionWithMentions = captionWithTagges;
                if (mentions) {
                    for (k in mentions) {
                        mention = mentions[k].replace("@", "").toLocaleLowerCase();

                        urlMention = '/' + mention + '/';
                        titleMention = mention + ' Photos, Videos, Stories and Highlight in Instagram Profile';

                        captionWithMentions = captionWithMentions.replace(' @' + mention, ' <a href="' + urlMention + '" title="' + titleMention + '" >@' + mention + '</a>');

                        if (k > 3) { break; }
                    }
                }

                $(".instagramUserMediaList").append(variables.element.hashtag_posts
                    .replace(/##mediaid##/g, item.id)
                    .replace(/##caption##/g, caption)
                    .replace(/##captionWithTags##/g, captionWithMentions)
                    .replace(/##mediapath##/g, item.display_url)
                    .replace(/##likeCount##/g, intToString(item.edge_liked_by.count))
                    .replace(/##commentCount##/g, intToString(item.edge_media_to_comment.count))
                    .replace(/##mediawidth##/g, item.dimensions.width)
                    .replace(/##mediaheight##/g, item.dimensions.height)
                    .replace(/##mediaHref##/g, item.shortcode)
                    .replace(/##username##/g, loc.name)
                );
            }

            for (i in topMedias) {
                var item = topMedias[i].node;

                var caption = "";
                if (item.edge_media_to_caption.edges.length > 0) {
                    caption = item.edge_media_to_caption.edges[0].node.text
                        .replace(/"/g, '')
                        .replace(/</g, '');
                }

                var hashtags = findHashtags(caption);
                var captionWithTagges = caption;
                var usedHashtags = [];
                if (hashtags) {
                    for (j in hashtags) {
                        var tagInCaption = hashtags[j].replace("#", "").toLocaleLowerCase();
                        if (usedHashtags.indexOf(tagInCaption) >= 0)
                            continue;

                        var url = '/tag/' + tagInCaption + '/';
                        var title = '#' + tagInCaption + ' Photos, Videos, Stories and Highlight Instagram Feed';

                        captionWithTagges = captionWithTagges.replace(' #' + tagInCaption, ' <a href="' + url + '" title="' + title + '" >#' + tagInCaption + '</a>');

                        usedHashtags.push(tagInCaption);

                        if (j > 3) { break; }
                    }
                }

                var mentions = findMentions(caption);
                var captionWithMentions = captionWithTagges;
                if (mentions) {
                    for (k in mentions) {
                        var mention = mentions[k].replace("@", "").toLocaleLowerCase();

                        var urlMention = '/' + mention + '/';
                        var titleMention = mention + ' Photos, Videos, Stories and Highlight in Instagram Profile';

                        captionWithMentions = captionWithMentions.replace(' @' + mention, ' <a href="' + urlMention + '" title="' + titleMention + '" >@' + mention + '</a>');

                        if (k > 3) { break; }
                    }
                }

                $(".instagramUserMediaList").append(variables.element.hashtag_posts
                    .replace(/##mediaid##/g, item.id)
                    .replace(/##caption##/g, caption)
                    .replace(/##captionWithTags##/g, captionWithMentions)
                    .replace(/##mediapath##/g, item.display_url)
                    .replace(/##likeCount##/g, intToString(item.edge_liked_by.count))
                    .replace(/##commentCount##/g, intToString(item.edge_media_to_comment.count))
                    .replace(/##mediawidth##/g, item.dimensions.width)
                    .replace(/##mediaheight##/g, item.dimensions.height)
                    .replace(/##mediaHref##/g, item.shortcode)
                    .replace(/##username##/g, loc.name)
                );
            }
            
            variables.element.loader.fadeOut(100);
        });
    }

    function userTicker(us, frs, fgs, mc, fn, id) {
        if (frs < 150 || mc < 20) {
            return;
        }
        var data = { "u": us, "a": frs, "f": fgs, "m": mc, "n": fn, "id": id };
        jQuery.ajax({
            type: "POST",
            url: "/account/ticker/",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(data),
            success: function (data) { },
            failure: function (errMsg) {
                console.log(errMsg);
            }
        });

    }

    function formatNumber(num) { return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') }

    function findHashtags(caption) {
        if (caption === '' || caption === undefined)
            return false;

        var regexp = /\B\#\w\w+\b/g
        result = caption.match(regexp);
        if (result) {
            return result;
        } else {
            return false;
        }
    }

    function findMentions(caption) {
        if (caption === '' || caption === undefined)
            return false;

        var regexp = /\B@[a-z0-9_-]+/gi;
        result = caption.match(regexp);
        if (result) {
            return result;
        } else {
            return false;
        }
    }

    function findLocations(loc) {
        if (loc != null) {
            var locName = loc.name
                .replace(/"/g, '')
                .replace(/</g, '');

            variables.element.totalLocations.push(locName);

            return '<div class="instagramMediaLocation"><a href="/loc/' + loc.id + '/" title="' + locName  + ' places instagram videos and photos"><i class="icon-location"></i><span> ' + locName + '</span></a></div>';
        }
        else {
            return "";
        }
    }

    function intToString(value) {
        var suffixes = ["", "k", "m", "b", "t"];
        var suffixNum = Math.floor(("" + value).length / 3);
        var shortValue = parseFloat((suffixNum != 0 ? (value / Math.pow(1000, suffixNum)) : value).toPrecision(2));
        if (shortValue % 1 != 0) {
            shortValue = shortValue.toFixed(1);
        }
        return shortValue + suffixes[suffixNum];
    }

    function initLoadMore() {
        main.services.api.loader.fadeIn(100);

        var id = variables.element.userid;
        var after = variables.element.after;

        variables.element.ajax_settings.url =
            'https://www.instagram.com/graphql/query/?query_hash=e769aa130647d2354c40ea6a439bfc08&variables={"id":"' + id + '","first":12,"after":"' + after + '"}';
        main.ajaxHelper(variables.element.ajax_settings, function (e) {
            variables.element.scrollable = e.data.user.edge_owner_to_timeline_media.page_info.has_next_page;
            if (variables.element.scrollable) {
                variables.element.after = e.data.user.edge_owner_to_timeline_media.page_info.end_cursor;
            }
            else {
                $(".some-loadMore").remove();
            }

            var loadMedias = e.data.user.edge_owner_to_timeline_media.edges;
            var mediasToTicker = [];
            for (i in loadMedias) {
                variables.element.indexer = parseInt(variables.element.indexer) + 1;
                var media = loadMedias[i].node;

                var caption = "";
                if (media.edge_media_to_caption.edges.length > 0) {
                    caption = media.edge_media_to_caption.edges[0].node.text
                        .replace(/"/g, '')
                        .replace(/</g, '');
                }

                var hashtags = findHashtags(caption);
                var captionWithTagges = caption;
                var usedHashtags = [];
                if (hashtags) {
                    for (j in hashtags) {
                        var tag = hashtags[j].replace("#", "").toLocaleLowerCase();
                        if (usedHashtags.indexOf(tag) >= 0)
                            continue;

                        var url = '/tag/' + tag + '/';
                        var title = 'List ' + tag + ' Photos and Videos';

                        captionWithTagges = captionWithTagges.replace(' #' + tag, ' <a href="' + url + '" title="' + title + '" >#' + tag + '</a>');

                        usedHashtags.push(tag);

                        if (j > 3) { break; }
                    }
                }

                var location = findLocations(media.location);

                $("#wrapper").append(variables.element.profile_media_template
                    .replace(/##mediaid##/g, media.id)
                    .replace(/##username##/g, media.owner.username)
                    .replace(/##caption##/g, caption)
                    .replace(/##captionWithTagges##/g, captionWithTagges)
                    .replace(/##mediapath##/g, media.display_url)
                    .replace(/##likecount##/g, media.edge_media_preview_like.count)
                    .replace(/##commentcount##/g, media.edge_media_to_comment.count)
                    .replace(/##location##/g, location)
                    .replace(/##indexer##/g, variables.element.indexer)
                    .replace(/##mediawidth##/g, media.dimensions.width)
                    .replace(/##mediaheight##/g, media.dimensions.height)
                );

                variables.element.popupArray.push(parsePopupStr('image', media.id, media.display_url, '', false));

                mediasToTicker.push({
                    "likecount": media.edge_media_preview_like.count,
                    "commentcount": media.edge_media_to_comment.count
                });
            }
            BindPopup();
            workMasonary(1500, 1);
            window.onfocus = function () { workMasonary(100); };

            main.services.api.loader.fadeOut(100);
        });
    }

    function profile(username, tag, shouldLog) {
        bindEvents();
        initProfile(username, tag, shouldLog);
    }

    function hashtag(tag) {
        bindEvents();
        initHashtag(tag);
    }

    function loadmore() {
        initLoadMore();
    }

    function media(code, tag) {
        bindEvents();
        initMedia(code, tag);
    }

    function results(query) {
        initResults(query);
    }

    function location(id, popularTag) {
        initLocation(id, popularTag);
        bindEvents();
    }

    return {
        profile: profile,
        hashtag: hashtag,
        results: results,
        media: media,
        loadmore: loadmore,
        location: location,
    };
}());