﻿namespace Instagram.Controllers
{
    using Instagram.Analysis.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class SearchController : Controller
    {
        public ActionResult Index(string param)
        {
            var model = new SearchViewModel();

            if (string.IsNullOrEmpty(param))
                return View(model);

            model = new SearchViewModel()
            {
                Param = param,
                IsEnableAds = false,
            };

            return View(model);
        }
    }
}