﻿namespace Instagram.Analysis.Controllers
{
    using Instagram.Analysis.Helpers;
    using Instagram.Analysis.Models;
    using Instagram.Data;
    using System.Web.Mvc;

    public class HomeController : Controller
    {
        PopularAccountData popularAccountData;
        HashtagData hashtagData;

        public HomeController()
        {
            popularAccountData = new PopularAccountData();
            hashtagData = new HashtagData();
        }

        public ActionResult Index()
        {
            var populars = popularAccountData.GetRandom(x => x.Followers > 1000000, 15);
            var hashtags = hashtagData.GetRandom(16);

            var model = new HomeViewModel()
            {
                Categories = CacheHelper.Categories,
                Hashtags = hashtags,
                PopularAccounts = populars
            };

            return View(model);
        }
    }
}