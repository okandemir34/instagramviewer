﻿namespace Instagram.Controllers
{
    using Instagram.Data;
    using Instagram.Analysis.Helpers;
    using Instagram.Analysis.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Text;
    using Newtonsoft.Json;

    public class AccountController : Controller
    {
        LogData _logData;

        public AccountController()
        {
            _logData = new LogData();
        }

        public ActionResult Index(string username)
        {
            var ban = CacheHelper.BanUsers.Where(x => x.Value == username).FirstOrDefault();
            if (ban != null)
                return RedirectToAction("Index", "Home", new { q = "user-is-block" });

            if (string.IsNullOrEmpty(username))
                return RedirectToAction("Index", "Home", new { q = "username-empty" });

            var logs = _logData.GetBy(x => x.Username == username);

            var model = new AccountViewModel()
            {
                IsEnableAds = true,
                Username = username,
            };

            if (logs.Count > 0)
            {
                foreach (var item in logs)
                {
                    model.Date.Add(item.CreateDate.ToString("dd.MM.yyyy"));
                    model.Followers.Add(item.Followers);
                    model.Friends.Add(item.Followings);
                    model.Media.Add(item.Medias);
                }
            }
            else
            {
                model.Date.Add(DateTime.Now.ToString("dd.MM.yyyy"));
                model.Followers.Add(0);
                model.Friends.Add(0);
                model.Media.Add(0);
            }

            return View(model);
        }

        public ActionResult Following(string username)
        {
            /*Redirect*/
            if (string.IsNullOrEmpty(username))
                return Redirect("http://hotsta.net");
            else
                return Redirect("http://hotsta.net/" + username + "/");
        }

        public ActionResult Followers(string username)
        {
            /*Redirect*/
            if (string.IsNullOrEmpty(username))
                return Redirect("http://hotsta.net");
            else
                return Redirect("http://hotsta.net/" + username + "/");
        }

        public ActionResult IdRoute(string username, string id)
        {
            /*Redirect*/
            if (string.IsNullOrEmpty(username))
                return Redirect("http://hotsta.net");
            else
                return Redirect("http://hotsta.net/" + username + "/");
        }

        [HttpPost]
        public JsonResult Ticker(UserModel model)
        {
            
            var dtNow = DateTime.Now;
            var logs = _logData.GetCount(x => x.UserId == model.id);

            if(logs > 7)
            {
                var first = _logData.GetBy(x => x.UserId == model.id).FirstOrDefault();
                _logData.DeleteByKey(first.Id);
            }
            else
            {
                var today = _logData.GetCount(x => x.UserId == model.id
                    && x.CreateDate.Day == dtNow.Day
                    && x.CreateDate.Month == dtNow.Month
                    && x.CreateDate.Year == dtNow.Year
                );

                if (today > 0)
                    return Json(false, JsonRequestBehavior.AllowGet);

                var logModel = new Model.Log()
                {
                    Followers = model.a,
                    Followings = model.f,
                    Fullname = model.n,
                    Medias = model.m,
                    UserId = model.id,
                    Username = model.u,
                    CreateDate = dtNow,
                };

                _logData.Insert(logModel);
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Tracker(UserTxtFileModel model)
        {
            var file = Server.MapPath("~/logs/user/" + model.us + ".txt");
            if (System.IO.File.Exists(file))
            {
                System.IO.File.WriteAllText(file, model.tx, Encoding.UTF8);
            }
            else
            {
                var newFile = Server.MapPath("~/logs/user/");
                System.IO.File.WriteAllText(newFile + model.us + ".txt", model.tx, Encoding.UTF8);
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ValidateInput(false)]
        public ActionResult Ban(string username)
        {
            if (string.IsNullOrEmpty(username))
                return RedirectToAction("Index", "Home", new { q = "username-empty" });

            var model = new Model.BanUser()
            {
                Description = "ban",
                Value = username,
            };

            CacheHelper.BanUsers.Add(model);

            var result = new Data.BanUserData().Insert(model);
            if (result.IsSucceed)
                return RedirectToAction("Index", "Home", new { q = "user_banned" });
            else
                return RedirectToAction("Index", "Home", new { q = "user_error_banned" });
        }
    }
}