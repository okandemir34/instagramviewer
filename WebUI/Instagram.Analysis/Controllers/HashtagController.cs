﻿namespace Instagram.Controllers
{
    using Instagram.Analysis.Helpers;
    using Instagram.Analysis.Models;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;

    public class HashtagController : Controller
    {
        public ActionResult Index(string hashtag)
        {
            var ban = CacheHelper.BanHashtags.Where(x => x.Value == hashtag).FirstOrDefault();
            if (ban != null)
                return RedirectToAction("Index", "Home", new { q = "hashtag-is-block" });

            if (string.IsNullOrEmpty(hashtag))
                return RedirectToAction("Index", "Home", new { q = "hashtag-empty" });

            var model = new HashtagViewModel()
            {
                Hashtag = hashtag,
                IsEnableAds = false,
            };

            return View(model);
        }

        [HttpGet]
        [ValidateInput(false)]
        public ActionResult Ban(string hashtag, int type)
        {
            if (string.IsNullOrEmpty(hashtag))
                return RedirectToAction("Index", "Home", new { q = "hashtag-empty" });

            var model = new Model.BanHashtag()
            {
                Description = "link ban",
                Value = hashtag,
                Type = type,
            };

            CacheHelper.BanHashtags.Add(model);

            var result = new Data.BanHashtagData().Insert(model);
            if (result.IsSucceed)
                return RedirectToAction("Index", "Home", new { q = "hashtag_banned" });
            else
                return RedirectToAction("Index", "Home", new { q = "error_hashtag_banned" });
        }

        [HttpPost]
        public JsonResult Tracker(UserTxtFileModel model)
        {
            var file = Server.MapPath("~/logs/hashtag/" + model.us + ".txt");
            if (System.IO.File.Exists(file))
            {
                System.IO.File.WriteAllText(file, model.tx, Encoding.UTF8);
            }
            else
            {
                var newFile = Server.MapPath("~/logs/hashtag/");
                System.IO.File.WriteAllText(newFile + model.us + ".txt", model.tx, Encoding.UTF8);
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }
    }
}