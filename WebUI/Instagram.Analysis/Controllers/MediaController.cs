﻿namespace Instagram.Controllers
{
    using Instagram.Analysis.Helpers;
    using Instagram.Analysis.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class MediaController : Controller
    {
        // GET: Media
        public ActionResult Index(string code)
        {
            var ban = CacheHelper.BanMedias.Where(x => x.Value == code).FirstOrDefault();
            if (ban != null)
                return RedirectToAction("Index", "Home", new { q = "media-is-block" });

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");

            if (string.IsNullOrEmpty(code))
                return RedirectToAction("Index", "Home", new { q = "media-code-empty" });

            var model = new MediaViewModel()
            {
                Code = code,
                IsEnableAds = false,
            };

            return View(model);
        }

        [HttpGet]
        [ValidateInput(false)]
        public ActionResult Ban(string code, int type)
        {
            if (string.IsNullOrEmpty(code))
                return RedirectToAction("Index", "Home", new { q = "code-empty" });

            var model = new Model.BanMedia()
            {
                Description = "ban",
                Value = code,
            };

            CacheHelper.BanMedias.Add(model);

            var result = new Data.BanMediaData().Insert(model);
            if (result.IsSucceed)
                return RedirectToAction("Index", "Home", new { q = "media_banned" });
            else
                return RedirectToAction("Index", "Home", new { q = "media_error_banned" });
        }
    }
}