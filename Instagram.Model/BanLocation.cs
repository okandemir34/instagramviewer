﻿namespace Instagram.Model
{
    public class BanLocation : Core.ModelBase
    {
        public string Value { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
    }
}
