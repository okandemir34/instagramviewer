﻿namespace Instagram.Model
{
    public class Setting : Core.ModelBase
    {
        public string Sitename { get; set; }
        public string Siteadres { get; set; }
        public string Analytics { get; set; }
        public string Ads300 { get; set; }
        public string Ads600 { get; set; }
        public bool IsTricker { get; set; }
    }
}
