﻿namespace Instagram.Model
{
    public class Hashtag : Core.ModelBase
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
