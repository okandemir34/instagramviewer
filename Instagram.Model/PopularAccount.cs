﻿namespace Instagram.Model
{
    public class PopularAccount : Core.ModelBase
    {
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string ImagePath { get; set; }
        public int Followers { get; set; }
        public int Followings { get; set; }
        public int Media { get; set; }
        public int CategoryId { get; set; }
        public bool IsActive { get; set; }
    }
}
