﻿namespace Instagram.Model
{
    public class Category : Core.ModelBase
    {
        public string Name { get; set; }
        public string Slug { get; set; }
        public bool IsActive { get; set; }
    }
}
