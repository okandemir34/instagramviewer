﻿using System;

namespace Instagram.Model.Core
{
    public class IgnoreAttribute : Attribute
    {
        public string SomeProperty { get; set; }
    }
}
