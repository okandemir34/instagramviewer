﻿namespace Instagram.Model
{
    public class Remove : Core.ModelBase
    {
        public string Fullname { get; set; }
        public string Username { get; set; }
        public string Mail { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}
