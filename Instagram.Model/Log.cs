﻿namespace Instagram.Model
{
    using System;

    public class Log : Core.ModelBase
    {
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string UserId { get; set; }
        public int Followers { get; set; }
        public int Followings { get; set; }
        public int Medias { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
