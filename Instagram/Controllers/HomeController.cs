﻿using HtmlAgilityPack;
using Instagram.Data;
using Instagram.Helpers;
using Instagram.Model;
using Instagram.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Instagram.Controllers
{
    public class HomeController : Controller
    {
        PopularAccountData _popularAccountData;
        HashtagData _hashtagData;
        CategoryData _categoryData;

        public HomeController()
        {
            _popularAccountData = new PopularAccountData();
            _hashtagData = new HashtagData();
            _categoryData = new CategoryData();
        }

        public ActionResult Index()
        {
            //return Redirect("http://hotsta.net");
            return View();
        }

        public ActionResult ClearBans()
        {
            CacheHelper.ClearBans();
            return RedirectToAction("Index", "Home", new { q = "clear_ban_cache" });
        }

        public ActionResult AddCategory()
        {
            var url = $"https://bigsta.net/categories/";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;

            var latestContentHtml = client.DownloadString(url);
            var latestContentDoc = new HtmlDocument();
            latestContentDoc.LoadHtml(latestContentHtml);

            var row = latestContentDoc.DocumentNode.SelectNodes("//a[@class='some-item-user']//strong");

            foreach (var item in row)
            {
                try
                {
                    var catModel = new Model.Category()
                    {
                        Name = item.InnerText,
                        IsActive = true,
                    };

                    _categoryData.Insert(catModel);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            return RedirectToAction("Hashtags", "Home");
        }

        public ActionResult Hashtags()
        {
            for (int i = 0; i <= 10; i++)
            {
                changeIP();
                var url = $"https://bigsta.net/trend/tags/";
                WebClient client = new WebClient();
                client.Encoding = Encoding.UTF8;

                var latestContentHtml = client.DownloadString(url);
                var latestContentDoc = new HtmlDocument();
                latestContentDoc.LoadHtml(latestContentHtml);

                var row = latestContentDoc.DocumentNode.SelectNodes("//div[@class='wrap']//strong");

                foreach (var item in row)
                {
                    try
                    {
                        var check = _hashtagData.GetBy(x => x.Name == item.InnerText).FirstOrDefault();
                        if(check == null)
                        {
                            var tagModel = new Model.Hashtag()
                            {
                                Name = item.InnerText,
                                Count = 0,
                            };

                            _hashtagData.Insert(tagModel);
                        }
                        else
                        {
                            continue;
                        }
                    }
                    catch (Exception ex)
                    {

                        throw;
                    }
                }
            }

            return RedirectToAction("AddPopular", "Home");
        }

        public JsonResult AddPopular()
        {
            for (int i = 2; i <= 15; i++)
            {
                changeIP();
                var url = $"https://bigsta.net/trend/accounts/{i}";
                WebClient client = new WebClient();
                client.Encoding = Encoding.UTF8;

                var latestContentHtml = client.DownloadString(url);
                var latestContentDoc = new HtmlDocument();
                latestContentDoc.LoadHtml(latestContentHtml);

                var row = latestContentDoc.DocumentNode.SelectNodes("//a[@class='some-item-user']");

                foreach (var item in row)
                {
                    try
                    {
                        var html = item.Attributes[1].Value;
                        var usernameSplit = html.Split('/');
                        var username = usernameSplit[2];
                        var checkUser = _popularAccountData.GetBy(x => x.Username == username).FirstOrDefault();
                        if(checkUser == null)
                        {
                            var instaUrl = "https://instagram.com/" + username + "?__a=1";
                            var insta = client.DownloadString(instaUrl);

                            var json = JsonConvert.DeserializeObject<InstaAccountModel>(insta);

                            if (json.Graphql.User != null)
                            {
                                var userModel = new PopularAccount()
                                {
                                    Followers = json.Graphql.User.Followers.Count,
                                    Followings = json.Graphql.User.Followings.Count,
                                    Fullname = json.Graphql.User.Fullname,
                                    ImagePath = json.Graphql.User.Avatar,
                                    Media = json.Graphql.User.Media.Count,
                                    Username = json.Graphql.User.Username,
                                    IsActive = true,
                                };

                                var result = _popularAccountData.Insert(userModel);
                            }
                        }
                        else
                        {
                            continue;
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteUsers()
        {
            var populars = _popularAccountData.GetAll();
            foreach (var item in populars)
            {
                if (item.Followers <= 1000000)
                    _popularAccountData.DeleteByKey(item.Id);
                else
                    continue;
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult X()
        {
            var list = new List<string>() { "t=actress&p=10&c=2", "t=author&p=2&c=3", "t=basketball&p=1&c=4", "t=beauty-fashion&p=7&c=5", "t=brands&p=3&c=6", "t=cars-motorbikes&p=3&c=7", "t=children-family&p=1&c=8", "t=clothes-accessories&p=5&c=9", "t=comedian&p=2&c=10", "t=dancer&p=5&c=11", "t=dj&p=8&c=12", "t=education&p=1&c=13", "t=entertainment&p=3&c=14", "t=fitness-yoga&p=2&c=15", "t=foods-grocery&p=8&c=16", "t=football&p=2&c=17", "t=home-garden&p=2&c=18", "t=how-to-style&p=1&c=19", "t=internet-personality&p=3&c=20", "t=luxury&p=1&c=21", "t=magazine&p=2&c=22", "t=models&p=9&c=23", "t=movies-tv&p=23&c=24", "t=music&p=24&c=25", "t=pets&p=1&c=26", "t=photography&p=5&c=27", "t=politics&p=2&c=28", "t=sport-clubs&p=1&c=29", "t=tattoo&p=2&c=31", "t=travel-tourism&p=2&c=33", "t=video-games&p=1&c=34" };
            for (int i = 0; i <= list.Count; i++)
            {
                var ur = $"http://localhost:55562/Home/AddCategoryUser?{list[i]}";
                WebClient wc = new WebClient();
                wc.DownloadString(ur);
            }

            return RedirectToAction("Index", "Home");
        }

        public JsonResult AddCategoryUser(string t, int p, int c)
        {
            var allUsers = _popularAccountData.GetAll();
            var list = new List<string>() { "t=pets&p=1&c=26", "t=photography&p=5&c=27", "t=politics&p=2&c=28", "t=sport-clubs&p=1&c=29", "t=tattoo&p=2&c=31", "t=travel-tourism&p=2&c=33", "t=video-games&p=1&c=34" };
            for (int l = 0; l < list.Count; l++)
            {
                var spData = list[l].Split('&');
                string page = spData[1].Replace("p=", "");
                string cat = spData[2].Replace("c=", "");
                p = Int32.Parse(page);
                c = Int32.Parse(cat);
                t = spData[0].Replace("t=", "");
                for (int i = 1; i <= p; i++)
                {
                    changeIP();
                    WebClient wc = new WebClient();
                    wc.Encoding = Encoding.UTF8;
                    var url = $"https://bigsta.net/category/{t}/{i}/";
                    var latestContentHtml = wc.DownloadString(url);
                    var latestContentDoc = new HtmlDocument();
                    latestContentDoc.LoadHtml(latestContentHtml);

                    var row = latestContentDoc.DocumentNode.SelectNodes("//div[@class='some-countryTable']//div[@class='user']//a");

                    foreach (var item in row)
                    {
                        try
                        {
                            var html = item.Attributes[0].Value;
                            var usernameSplit = html.Split('/');
                            var username = usernameSplit[2];
                            
                            var checkUser = allUsers.Where(x => x.Username == username).FirstOrDefault();
                            if (checkUser == null)
                            {
                                var instaUrl = "https://instagram.com/" + username + "?__a=1";
                                var insta = wc.DownloadString(instaUrl);

                                var json = JsonConvert.DeserializeObject<InstaAccountModel>(insta);

                                if (json.Graphql.User != null)
                                {
                                    var userModel = new PopularAccount()
                                    {
                                        Followers = json.Graphql.User.Followers.Count,
                                        Followings = json.Graphql.User.Followings.Count,
                                        Fullname = json.Graphql.User.Fullname,
                                        ImagePath = json.Graphql.User.Avatar,
                                        Media = json.Graphql.User.Media.Count,
                                        Username = json.Graphql.User.Username,
                                        CategoryId = c,
                                        IsActive = true,
                                    };

                                    var result = _popularAccountData.Insert(userModel);
                                    allUsers.Add(userModel);
                                }
                            }
                            else
                            {
                                checkUser.CategoryId = c;
                                _popularAccountData.Update(checkUser);
                            }
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                    }
                }
            }


            
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public void changeIP()
        {
            new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "cmd.exe",
                    Arguments = "/C adb shell svc data disable; svc data enable;"
                }
            }.Start();
            string ipAdresi = "";
            for (int di = 1; di <= 1; di++)
            {
                try
                {
                    ipAdresi = (new System.Net.WebClient()).DownloadString("http://checkip.dyndns.org/");
                    ipAdresi = (new System.Text.RegularExpressions.Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")).Matches(ipAdresi)[0].ToString();
                    Thread.Sleep(2000);
                }
                catch
                {
                    Thread.Sleep(5000);
                    di = 0;
                    continue;
                }
            }
        }
    }
}