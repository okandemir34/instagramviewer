﻿namespace Instagram.Controllers
{
    using Instagram.Data;
    using Instagram.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class ListController : Controller
    {
        PopularAccountData _popularAccountData;
        CategoryData _categoryData;
        HashtagData _hashtagData;

        public ListController()
        {
            _popularAccountData = new PopularAccountData();
            _categoryData = new CategoryData();
            _hashtagData = new HashtagData();
        }

        public ActionResult Index(int page = 1)
        {
            //return Redirect("http://hotsta.net/popular-accounts/");

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");

            var populars = _popularAccountData.GetByPage(page, 100, "Followers", true);

            var model = new ListViewModel()
            {
                PopularAccounts = populars,
                Page = page,
            };

            return View(model);
        }

        public ActionResult Category()
        {
            //return Redirect("http://hotsta.net/popular-categories/");

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");

            var categories = _categoryData.GetAll();

            var model = new ListCategoryViewModel()
            {
                Categories = categories
            };

            return View(model);
        }

        public ActionResult Hashtag()
        {
            //return Redirect("http://hotsta.net/popular-hashtags/");

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");

            var hashtags = _hashtagData.GetAll();

            var model = new ListHashtagViewModel()
            {
                Hashtags = hashtags
            };

            return View(model);
        }

        public ActionResult Detail(string slug, int page = 1)
        {
            //return Redirect("http://hotsta.net/categories/");

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");

            if (string.IsNullOrEmpty(slug))
                return RedirectToAction("Index", "Home", new { q = "slug-is-empty" });

            var cat = _categoryData.GetBy(x => x.Slug == slug).FirstOrDefault();
            if (cat == null)
                return RedirectToAction("Index", "Home", new { q = "not-found-category", e = slug });
            
            var populars = _popularAccountData.GetByPage(x=>x.CategoryId == cat.Id, page, 100, "Followers", true);

            var model = new DetailViewModel()
            {
                PopularAccounts = populars,
                Page = page,
                Category= cat,
            };

            return View(model);
        }
    }
}