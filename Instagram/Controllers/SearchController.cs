﻿namespace Instagram.Controllers
{
    using Instagram.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class SearchController : Controller
    {
        public ActionResult Index(string param)
        {
            /*if (string.IsNullOrEmpty(param))
                return Redirect("http://hotsta.net/search/");
            else
                return Redirect("http://hotsta.net/search/" + param + "/");*/

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");

            var model = new SearchViewModel();
            if (string.IsNullOrEmpty(param))
                return View(model);

            model = new SearchViewModel()
            {
                Param = param,
                IsEnableAds = false,
            };

            return View(model);
        }
    }
}