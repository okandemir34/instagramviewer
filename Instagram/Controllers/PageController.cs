﻿namespace Instagram.Controllers
{
    using Instagram.Data;
    using Instagram.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class PageController : Controller
    {
        public ActionResult Index(string username)
        {
            //return Redirect("http://hotsta.net/report-a-problem/");

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");

            if (string.IsNullOrEmpty(username))
                username = "";

            Random rnd = new Random();
            
            var model = new ReportModel()
            {
                Fullname = "",
                MailAddress = "",
                Message = "",
                Username = username,
                Subject = "",
                Captha1 = rnd.Next(1,9),
                Captha2 = rnd.Next(1,9),
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(string fullname, string mail, string message, string username, string subject, int capt1, int capt2, int captcha)
        {
            int capResult = capt1 + capt2;
            var errorList = new List<string>();
            if (captcha <= 0)
                errorList.Add("Captcha empty");
            if (captcha != capResult)
                errorList.Add("Captcha Error");
            if(string.IsNullOrEmpty(fullname))
                errorList.Add("Fullname empty");
            if (string.IsNullOrEmpty(mail))
                errorList.Add("EMail empty");
            if (string.IsNullOrEmpty(message))
                errorList.Add("Message empty");
            if (string.IsNullOrEmpty(username))
                errorList.Add("Username empty");
            if (string.IsNullOrEmpty(subject))
                errorList.Add("Subject empty");

            Random rnd = new Random();

            var model = new ReportModel()
            {
                Fullname = fullname,
                MailAddress = mail,
                Message = message,
                Subject = subject,
                Username = username,
                Captha1 = rnd.Next(1, 9),
                Captha2 = rnd.Next(1, 9),
            };

            var insertModel = new Model.Remove()
            {
                Fullname = fullname,
                Mail = mail,
                Message = message,
                Subject = subject,
                Username = username,
                IsActive = true,
                IsDeleted = false,
            };

            if(errorList.Count > 0)
            {
                ViewBag.ErrorList = errorList;
                return View(model);
            }

            var _data = new RemoveData();
            var check = _data.GetBy(x => x.Username == username && x.IsActive && !x.IsDeleted).FirstOrDefault();
            if(check != null)
            {
                errorList.Add("You already progress...");
                ViewBag.ErrorList = errorList;
                return View(model);
            }
            _data.Insert(insertModel);

            ViewBag.Success = "ss";
            
            return View(model);
        }

        public ActionResult About()
        {
            //return Redirect("http://hotsta.net/about/");

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");

            return View();
        }

        public ActionResult Team()
        {
            //return Redirect("http://hotsta.net/team/");

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");

            return View();
        }

        public ActionResult Company()
        {
            //return Redirect("http://hotsta.net/company/");

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");

            return View();
        }

        public ActionResult Privacy()
        {
            //return Redirect("http://hotsta.net/privacy-policy/"); 

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");

            return View();
        }

    }
}