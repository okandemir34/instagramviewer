﻿namespace Instagram.Controllers
{
    using Instagram.Helpers;
    using Instagram.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class HashtagController : Controller
    {
        public ActionResult Index(string hashtag)
        {
            var ban = CacheHelper.BanHashtags.Where(x => x.Value == hashtag).FirstOrDefault();
            if (ban != null)
                return RedirectToAction("Index", "Home", new { q = "hashtag-is-block" });

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");


            if (string.IsNullOrEmpty(hashtag))
                return RedirectToAction("Index", "Home", new { q = "hashtag-empty" });

            var model = new HashtagViewModel()
            {
                Hashtag = hashtag,
                IsEnableAds = false,
            };

            return View(model);
        }

        [HttpGet]
        [ValidateInput(false)]
        public ActionResult Ban(string hashtag)
        {
            if (string.IsNullOrEmpty(hashtag))
                return RedirectToAction("Index", "Home", new { q = "hashtag-empty" });

            var model = new Model.BanHashtag()
            {
                Description = "ban",
                Value = hashtag,
            };

            CacheHelper.BanHashtags.Add(model);

            var result = new Data.BanHashtagData().Insert(model);
            if (result.IsSucceed)
                return RedirectToAction("Index", "Home", new { q = "hashtag_banned" });
            else
                return RedirectToAction("Index", "Home", new { q = "error_hashtag_banned" });
        }
    }
}