﻿namespace Instagram.Controllers
{
    using Instagram.Helpers;
    using Instagram.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class LocationController : Controller
    {
        // GET: Location
        public ActionResult Index(string id)
        {
            //return Redirect("http://hotsta.net/loc/"+id+"/");

            var ban = CacheHelper.BanLocations.Where(x => x.Value == id).FirstOrDefault();
            if (ban != null)
                return RedirectToAction("Index", "Home", new { q = "location-is-block" });

            if (!Request.Path.EndsWith("/"))
                return RedirectPermanent(Request.Url.ToString() + "/");

            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index", "Home", new { q = "location-empty" });

            var model = new LocationViewModel()
            {
                Location = id,
                IsEnableAds = false,
            };

            return View(model);
        }

        public ActionResult Geo(string id)
        {
            return Redirect("http://hotsta.net/loc/" + id + "/");
        }

        [HttpGet]
        [ValidateInput(false)]
        public ActionResult Ban(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index", "Home", new { q = "id-empty" });

            var model = new Model.BanLocation()
            {
                Description = "ban",
                Value = id,
            };

            CacheHelper.BanLocations.Add(model);

            var result = new Data.BanLocationData().Insert(model);
            if (result.IsSucceed)
                return RedirectToAction("Index", "Home", new { q = "location_banned" });
            else
                return RedirectToAction("Index", "Home", new { q = "location_error_banned" });
        }
    }
}