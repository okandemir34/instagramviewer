﻿namespace Instagram.Models
{
    using Instagram.Model;
    using System.Collections.Generic;

    public class ListCategoryViewModel
    {
        public ListCategoryViewModel()
        {
            Categories = new List<Category>();
        }

        public List<Category> Categories { get; set; }
    }
}