﻿namespace Instagram.Models
{
    public class MediaViewModel
    {
        public MediaViewModel()
        {
            IsEnableAds = true;
        }

        public bool IsEnableAds { get; set; }
        public string Code { get; set; }
    }
}