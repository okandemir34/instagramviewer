﻿namespace Instagram.Models
{
    using Instagram.Model;
    using System.Collections.Generic;

    public class ListHashtagViewModel
    {
        public ListHashtagViewModel()
        {
            Hashtags = new List<Hashtag>();
        }

        public List<Hashtag> Hashtags { get; set; }
    }
}