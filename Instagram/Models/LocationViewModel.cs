﻿namespace Instagram.Models
{
    public class LocationViewModel
    {
        public LocationViewModel()
        {
            IsEnableAds = true;
        }

        public bool IsEnableAds { get; set; }
        public string Location { get; set; }
    }
}