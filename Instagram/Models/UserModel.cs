﻿using Newtonsoft.Json;

namespace Instagram.Models
{
    public class UserModel
    {
        [JsonProperty("f")]
        public int f { get; set; }

        [JsonProperty("a")]
        public int a { get; set; }

        [JsonProperty("u")]
        public string u { get; set; }

        [JsonProperty("m")]
        public int m { get; set; }

        [JsonProperty("n")]
        public string n { get; set; }

        [JsonProperty("id")]
        public string id { get; set; }
    }
}