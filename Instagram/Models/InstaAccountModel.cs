﻿using Newtonsoft.Json;

namespace Instagram.Models
{
    public class InstaAccountModel
    {
        [JsonProperty("graphql")]
        public Graphql Graphql { get; set; }
    }

    public partial class Graphql
    {
        [JsonProperty("user")]
        public User User { get; set; }
    }

    public partial class User
    {
        [JsonProperty("edge_followed_by")]
        public Counted Followers { get; set; }
        
        [JsonProperty("edge_follow")]
        public Counted Followings { get; set; }

        [JsonProperty("edge_owner_to_timeline_media")]
        public Counted Media { get; set; }

        [JsonProperty("full_name")]
        public string Fullname { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
        
        [JsonProperty("profile_pic_url_hd")]
        public string Avatar { get; set; }
    }
    
    public partial class Counted
    {
        [JsonProperty("count")]
        public int Count { get; set; }
    }
}