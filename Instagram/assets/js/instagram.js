﻿const main = (function () {

    const services = {
        "api": {
            "base": "https://www.instagram.com/",
            "loader": $(".loader"),
        }
    };

    const ajaxHelper = function (settings, callback) {
        var returnOBJ = {};
        $.ajax(settings)
            .done(function (e) {
                returnOBJ.error = false;
                returnOBJ.code = "";
                returnOBJ.data = e;
                callback(e);
            })
            .fail(function (e) {
                returnOBJ.error = true;
                returnOBJ.code = "1";
                returnOBJ.data = "";
                callback(e);
            });
    };

    return {
        ajaxHelper: ajaxHelper,
        services: services,
    }
}());

const instagram = (function () {

    const variables = {
        "element": {
            "api_base": main.services.api.base,
            "loader": $(".loader"),
            "account_header": '<div class="wrapper">' +
                '<img class="img-circle-big" src="##avatar##" ' +
                'width="100%" height="100%" alt="##fullname## Instagram Profile Picture ##username##" title="##fullname## Instagram Profile Picture ##username##">' +
                '<ul class="wrapper-list">' +
                '<li><strong>##fullname##</strong></li>' +
                '<li><span class="site--color-light">##username##</span></li>' +
                '<li>##biography##</li>' +
                '<li>##externalurl##</li>' +
                '<li>' +
                '<div class="spacer"></div>' +
                '<span class="site-tag site-tag--error "><i class="ti-info-alt"></i> <a class="site--color-white" href="/report-a-problem/##username##/"><span> Report a problem</span></a></span>' +
                '</li>' +
                '</ul>' +
                '</div>',
            "small_text": '<i>Analytics of ##username##\'s Instagram Account</i>',
            "account_stats": '<div class="site-table site-table--striped d-table">' +
                '<div class="site-table-body">' +
                '<div class="site-table-row table-row-fix">' +
                '<div class="site-table-cell table-cell-fix"><strong>Full Name</strong></div>' +
                '<div class="site-table-cell">##fullname##</div>' +
                '</div>' +
                '<div class="site-table-row table-row-fix">' +
                '<div class="site-table-cell"><strong>User Name</strong></div>' +
                '<div class="site-table-cell">##username##</div>' +
                '</div>' +
                '<div class="site-table-row ">' +
                '<div class="site-table-cell w280"><strong>Is Verified</strong></div>' +
                '<div class="site-table-cell">##verified##</div>' +
                '</div>' +
                '<div class="site-table-row ">' +
                '<div class="site-table-cell"><strong>Followers</strong></div>' +
                '<div class="site-table-cell">##followersCount##</div>' +
                '</div>' +

                '<div class="site-table-row ">' +
                '<div class="site-table-cell"><strong>Following</strong></div>' +
                '<div class="site-table-cell">##followingCount##</div>' +
                '</div>' +
                '<div class="site-table-row ">' +
                '<div class="site-table-cell"><strong>Media Count</strong></div>' +
                '<div class="site-table-cell">##mediaCount##</div>' +
                '</div>' +
                '</div></div>',
            "jumbotron_subtitle": 'Reach analytical data of ##fullname## - @##username## Instagram profile. Browse Instagram photos and videos of @##username## and @##username## stories and highlights',
            "account_page_title": '##fullname## (@##username##) - Instagram metrics, photos and videos',
            "account_page_description": '##fullname## (@##username##) Instagram metrics, profile, photos and videos - ##biography##',
            "account_medias": '<div class="grid-item">' +
                '<div class="site-card">' +
                '<div class="site-card-header">' +
                '<div class="url-unstyled" title="##username## Instagram Account">' +
                '<div class="img-circle-left">' +
                '<img class="img-circle-small" src="" lazy-src="##avatar##" alt="##fullname## Instagram Account" title="##fullname## Instagram Account">' +
                '</div>' +
                '<div class="img-circle-right">' +
                '<span class="url-title-main">##fullname##</span>' +
                '<span class="url-title-sub">@##username##</span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="site-card-body div-relative">' +
                '<a href="/media/##mediaHref##/" title="##caption##" >' +
                '<img class="site--width-100" src="" lazy-src="##mediapath##" height="100%" width="100%" alt="" title="##caption##" alt="##caption##" />' +
                '</a>' +
                '<div class="card-padding">' +
                '<div class="card-stats">' +
                '<div class="stat">##likeCount## Like</div>' +
                '<div class="stat">##commentCount## Comment</div>' +
                '</div>' +
                '<p>##location##</p>' +
                '<p class="card-title">##captionWithTagges##</p>' +
                '</div>' +
                '</div>' +
                '<div class="site-card-footer centered">' +
                '<a href="/media/##mediaHref##/" title="##username## Instagram photo\'s detail" class="site--color-dark site-btn site-btn--light site-btn-small"><strong>More Details<i class="ti-arrow-top-right"></i></strong></a>' +
                '</div>' +
                '</div>' +
                '</div>',
            "hashtag_medias": '<div class="grid-item">' +
                '<div class="site-card">' +
                '<div class="site-card-body div-relative">' +
                '<a href="/media/##mediaHref##/" title="##caption##" >' +
                '<img class="site--width-100" src="" lazy-src="##mediapath##" height="100%" width="100%" alt="" title="##caption##" alt="##caption##" />' +
                '</a>' +
                '<div class="card-padding">' +
                '<div class="card-stats">' +
                '<div class="stat">##likeCount## Like</div>' +
                '<div class="stat">##commentCount## Comment</div>' +
                '</div>' +
                '<p class="card-title">##captionWithTagges##</p>' +
                '</div>' +
                '</div>' +
                '<div class="site-card-footer centered">' +
                '<a href="/media/##mediaHref##/" title="##username## Instagram photo\'s detail" class="site--color-dark site-btn site-btn--light site-btn-small"><strong>More Details<i class="ti-arrow-top-right"></i></strong></a>' +
                '</div>' +
                '</div>' +
                '</div>',
            "profile_stats_temp":'<div class="site-grid-col site-grid-col--3">'+
                                    '<div class="stats">'+
                                        '<p>##value##</p>'+
                                        '<strong>##name##</strong>'+
                                    '</div>'+
                                '</div>',
            "search_hashtag": '<div class="searchTagBox siteBox-1">' +
                '<a href="##hashtagHref##/" title="##hashtagTitle##">' +
                '<p class="hashtagName">##tag##</p>' +
                '<p class="hashtagCount">##count## Posts</p>' +
                '</a>' +
                '</div>',
            "search_user": '<div class="siteBox-3">' +
                '<div class="siteSearchUserBox">' +
                '<a href="##accountHref##" title="##accountTitle##">' +
                '<img src="##avatar##" alt="##accountTitle##" title="##accountTitle##">' +
                '<p class="fullname">##fullname##</p>' +
                '<p class="username">@##username##</p>' +
                '</a>' +
                '</div>' +
                '</div>',
            "doc_title": '<h1>Analytical data of ##fullname## (@##username##) Instagram profile</h1>',
            "totalLike": 0,
            "totalComment": 0,
            "totalMetion": 0,
            "totalHastags": 0,
            "lastShare":"",
            "lastLocation": "None",
            "totalMedia":0,
            "listMentions":[],
            "listHashtags":[],
            "listLocations": [],
            "topMedia":"",
            ajax_settings: {
                type: "GET"
            },
        }
    };

    function bindEvents() {
    }

    function initProfile(username, popularTag) {
        variables.element.ajax_settings.url = variables.element.api_base + username + "/?__a=1";
        main.ajaxHelper(variables.element.ajax_settings, function (e) {
            if (e.status === 404) {
                $("#docTitle").append("<p>@" + username + " 's Instagram Account does not exist!</p>");
                $("#docTitle").append('<p class="private-small">But you may be interested in this ' + popularTag + ' Photos and Videos</p>');
                return initHashtag(tag);
            }

            if (e.graphql == undefined) {
                var title = $(e).filter('title');
                if (title.length > 0) {
                    $("#docTitle").append("<p>" + title.text() + "</p>");
                    $("#docTitle").append('<p class="private-small">But you may be interested in this ' + popularTag + ' Photos and Videos</p>');
                    return initHashtag(tag);
                }
            }

            var user = e.graphql.user;

            if (user.is_private) {
                $("#docTitle").append('<p>@' + username + '\'s Instagram Account Is Private</p> <p class="private-small">But you may be interested in this ' + popularTag + ' Photos and Videos</p>');
                $("#docTitle").append('');
                $(".listUserInfos").remove();
                $("#statCont").remove();
                initHashtag(popularTag);
                return;
            }

            $("#accountHeader").append(variables.element.account_header
                .replace(/##avatar##/g, user.profile_pic_url_hd)
                .replace(/##username##/g, user.username)
                .replace(/##fullname##/g, user.full_name)
                .replace(/##biography##/g, user.biography)
                .replace(/##externalurl##/g, user.external_url)

            );

            $(".site-jumbotron-subtitle").html("Reach analytical data of " + user.full_name + " - @" + user.username + " Instagram profile. Browse Instagram photos and videos shares of @" + user.username + "or " + user.full_name + "stories and highlight");

            $("#docTitle").append(variables.element.doc_title
                .replace(/##username##/g, user.username)
                .replace(/##fullname##/g, user.full_name)
            );

            $("#accountHeader").append('<div class="spacer"></div><small class="text-muted"><i>Analytics of ' + user.username + '\'s Instagram Account</i></small>');

            $("#accountHeader").append(variables.element.account_stats
                .replace(/##followingCount##/g, formatNumber(user.edge_follow.count))
                .replace(/##mediaCount##/g, formatNumber(user.edge_owner_to_timeline_media.count))
                .replace(/##followersCount##/g, formatNumber(user.edge_followed_by.count))
                .replace(/##username##/g, user.username)
                .replace(/##fullname##/g, user.full_name)
                .replace(/##verified##/g, user.is_verified)
            );

            variables.element.totalMedia = user.edge_owner_to_timeline_media.count;

            if (user.full_name === "" || user.full_name === undefined)
                user.full_name = user.username;


            document.title = variables.element.account_page_title
                .replace(/##username##/g, user.username)
                .replace(/##fullname##/g, user.full_name);

            var pageDesc = variables.element.account_page_description
                .replace(/##username##/g, user.username)
                .replace(/##fullname##/g, user.full_name)
                .replace(/##biography##/g, user.biography);

            if (pageDesc.length > 160) {
                pageDesc = pageDesc.substr(0, 160);
            }

            $('meta[name="description"]').attr("content", pageDesc);

            if (user.edge_owner_to_timeline_media.count <= 4) {

                initHashtag(popularTag);
                $("#docTitle").remove();
                $(".centered").append('But you may be interested in this ' + popularTag + ' Photos and Videos')
                return;
            }

            var poplike = 0;
            var currentlike = 0;
            for (i in user.edge_owner_to_timeline_media.edges) {

                var media = user.edge_owner_to_timeline_media.edges[i].node;

                poplike = media.edge_liked_by.count;
                if (poplike > currentlike) {
                    variables.element.topMedia = media.display_url;
                }

                if (variables.element.lastShare === "") {
                    let unix_timestamp = media.taken_at_timestamp;
                    var date = new Date(unix_timestamp * 1000);
                    var day = date.getDay();
                    var month = date.getMonth();
                    var year = date.getFullYear();

                    month = month + 1;

                    variables.element.lastShare = day + "/" + month + "/" + year;
                }

                var caption = "";
                if (media.edge_media_to_caption.edges.length > 0) {
                    caption = media.edge_media_to_caption.edges[0].node.text
                        .replace(/"/g, '')
                        .replace(/</g, '');
                }

                var hashtags = findHashtags(caption);
                var captionWithTagges = caption;
                var usedHashtags = [];
                if (hashtags) {
                    for (j in hashtags) {

                        if (j > 3) {
                            variables.element.totalHastags = variables.element.totalHastags + 1;

                            var addlist = hashtags[j].replace("#", "").toLocaleLowerCase();
                            if (usedHashtags.indexOf(addlist) >= 0)
                                continue;

                            variables.element.listHashtags.push(addlist);

                            continue;
                        }

                        variables.element.totalHastags = variables.element.totalHastags + 1;

                        var tag = hashtags[j].replace("#", "").toLocaleLowerCase();
                        if (usedHashtags.indexOf(tag) >= 0)
                            continue;

                        variables.element.listHashtags.push(tag);

                        var url = '/tag/' + tag + '/';
                        var titleProfile = '#' + tag + ' Instagram Feed';

                        captionWithTagges = captionWithTagges.replace(' #' + tag, ' <a href="' + url + '" title="' + titleProfile + '" >#' + tag + '</a>');

                        usedHashtags.push(tag);
                    }
                }

                var mentions = findMentions(caption);
                var captionWithMentions = captionWithTagges;
                if (mentions) {
                    for (k in mentions) {
                        if (k > 3) {
                            variables.element.totalMetion = variables.element.totalMetion + 1;

                            var addMentions = mentions[k].replace("@", "").toLocaleLowerCase();
                            variables.element.listMentions.push(addMentions);

                            continue;
                        }

                        variables.element.totalMetion = variables.element.totalMetion + 1;

                        var mention = mentions[k].replace("@", "").toLocaleLowerCase();
                        variables.element.listMentions.push(mention);
                        var urlMention = '/' + mention + '/';
                        var titleMention = mention + ' Photos and Videos in Instagram Account';

                        captionWithMentions = captionWithMentions.replace(' @' + mention, ' <a href="' + urlMention + '" title="' + titleMention + '" >@' + mention + '</a>');

                        if (k > 3) { break; }
                    }
                }

                var location = findLocations(media.location);
                variables.element.indexer = i;
                
                variables.element.totalLike = variables.element.totalLike + media.edge_liked_by.count;
                variables.element.totalComment = variables.element.totalComment + media.edge_media_to_comment.count;

                $(".gallery").append(variables.element.account_medias
                    .replace(/##mediaid##/g, media.id)
                    .replace(/##username##/g, user.username)
                    .replace(/##caption##/g, user.username+" Media : " +caption)
                    .replace(/##captionWithTagges##/g, captionWithMentions)
                    .replace(/##mediapath##/g, media.display_url)
                    .replace(/##likeCount##/g, intToString(media.edge_liked_by.count))
                    .replace(/##commentCount##/g, intToString(media.edge_media_to_comment.count))
                    .replace(/##location##/g, location)
                    .replace(/##indexer##/g, i)
                    .replace(/##mediawidth##/g, media.dimensions.width)
                    .replace(/##mediaheight##/g, media.dimensions.height)
                    .replace(/##fullname##/g, user.full_name)
                    .replace(/##avatar##/g, user.profile_pic_url_hd)
                    .replace(/##mediaHref##/g, media.shortcode)
                );

                currentlike = media.edge_liked_by.count;
            }

            $(".profileStats").append(variables.element.profile_stats_temp
                .replace(/##name##/g, "Total Like")
                .replace(/##value##/g, formatNumber(variables.element.totalLike))
            );

            $(".profileStats").append(variables.element.profile_stats_temp
                .replace(/##name##/g, "Total Comment")
                .replace(/##value##/g, formatNumber(variables.element.totalComment))
            );

            $(".profileStats").append(variables.element.profile_stats_temp
                .replace(/##name##/g, "Last Location")
                .replace(/##value##/g, variables.element.lastLocation)
            );

            $(".profileStats").append(variables.element.profile_stats_temp
                .replace(/##name##/g, "Total Metions")
                .replace(/##value##/g, formatNumber(variables.element.totalMetion))
            );

            $(".profileStats").append(variables.element.profile_stats_temp
                .replace(/##name##/g, "Total Hashtags")
                .replace(/##value##/g, formatNumber(variables.element.totalHastags))
            );

            $(".profileStats").append(variables.element.profile_stats_temp
                .replace(/##name##/g, "Total Medias")
                .replace(/##value##/g, formatNumber(variables.element.totalMedia))
            );

            $(".profileStats").append(variables.element.profile_stats_temp
                .replace(/##name##/g, "Filter")
                .replace(/##value##/g, "Normal")
            );

            $(".profileStats").append(variables.element.profile_stats_temp
                .replace(/##name##/g, "Last Share")
                .replace(/##value##/g, variables.element.lastShare)
            );

            var mentionCount = 0;

            if (variables.element.listMentions.length === 0) {
                $(".mentions > ul").append("<li>Not Used Any Mentions</li>");
            }

            for (y in variables.element.listMentions) {
                mentionCount = mentionCount + 1;
                if (mentionCount === 10)
                    break;

                var it = variables.element.listMentions[y];
                if (it != null) {
                    $(".mentions > ul").append("<li>" + it + "</li>");
                }
            }

            var hashCount = 0;

            if (variables.element.listHashtags.length === 0) {
                $(".hashtags > ul").append("<li>Not Used Any Hashtags</li>");
            }

            for (z in variables.element.listHashtags) {
                hashCount = hashCount + 1;
                if (hashCount === 10)
                    break;

                var ht = variables.element.listHashtags[z];
                if (ht != null) {
                    $(".hashtags > ul").append("<li>" + ht + "</li>");
                }
            }

            var locCount = 0;

            if (variables.element.listLocations.length === 0) {
                $(".locations > ul").append("<li>None</li>");
            }

            for (x in variables.element.listLocations) {
                locCount = locCount + 1;
                if (locCount === 10)
                    break;

                var lc = variables.element.listLocations[x];
                if (lc != null) {
                    $(".locations > ul").append("<li>" + lc + "</li>");
                }
            }

            setInterval(function () {
                new Masonry('.gallery', {
                    itemSelector: '.grid-item'
                });
            }, 2000);

            if ('IntersectionObserver' in window) {
                var callback = function (entries, observer) {
                    entries.forEach(entry => {
                        if (entry.isIntersecting) {
                            var dataSrc = entry.target.getAttribute('lazy-src');
                            if (dataSrc == null || dataSrc == undefined) {
                                return;
                            }
                            if (dataSrc != '') {
                                entry.target.setAttribute('src', dataSrc);
                                entry.target.setAttribute('lazy-src', '');
                            }
                        }
                    });
                };
                var options = {
                    root: null,
                    rootMargin: '0px',
                    threshold: 0.01
                }

                var observer = new IntersectionObserver(callback, options);
                var images = document.querySelectorAll('.grid-item .site-card img');
                images.forEach(img => observer.observe(img));
            }

            userTicker(user.username, user.edge_followed_by.count, user.edge_follow.count, user.edge_owner_to_timeline_media.count, user.full_name, user.id);
            variables.element.loader.fadeOut(100);
        });
    }

    function initHashtag(tag) {
        variables.element.ajax_settings.url = variables.element.api_base + "explore/tags/" + tag + "/?__a=1&hl=en";
        main.ajaxHelper(variables.element.ajax_settings, function (e) {
            if (e.status === 404) {
                return initHashtag('nature');
            }

            var medias = e.graphql.hashtag.edge_hashtag_to_media.edges;
            if (medias.length == 0) {
                medias = e.graphql.hashtag.edge_hashtag_to_top_posts.edges;
            }

            $(".site-box-title").html("#" + tag);
            $(".centered > h1").html("Browse Instagram " + tag + " ideas");
            $(".centered > h2").html("Browse Instagram " + tag + " ideas, Browse " + tag + " photos, videos and " + tag + " medias");

            for (i in medias) {

                var media = medias[i].node;
                var caption = "";
                if (media.edge_media_to_caption.edges.length > 0) {
                    caption = media.edge_media_to_caption.edges[0].node.text
                        .replace(/"/g, '')
                        .replace(/</g, '');
                }

                var hashtags = findHashtags(caption);
                var captionWithTagges = caption;
                var usedHashtags = [];
                usedHashtags.push(tag);
                if (hashtags) {
                    for (j in hashtags) {
                        var tagInCaption = hashtags[j].replace("#", "").toLocaleLowerCase();
                        if (usedHashtags.indexOf(tagInCaption) >= 0 || tag == tagInCaption)
                            continue;

                        var url = '/tag/' + tagInCaption + '/';
                        var title = '#' + tag + ' Instagram photos, videos and stories';

                        captionWithTagges = captionWithTagges.replace(' #' + tagInCaption, ' <a href="' + url + '" title="' + title + '" >#' + tagInCaption + '</a>');

                        usedHashtags.push(tagInCaption);

                        if (j > 3) { break; }
                    }
                }

                var mentions = findMentions(caption);
                var captionWithMentions = captionWithTagges;
                if (mentions) {
                    for (k in mentions) {
                        var mention = mentions[k].replace("@", "").toLocaleLowerCase();

                        var urlMention = '/' + mention + '/';
                        var titleMention = mention + ' Photos in Instagram Account';

                        captionWithMentions = captionWithMentions.replace(' @' + mention, ' <a href="' + urlMention + '" title="' + titleMention + '" >@' + mention + '</a>');

                        if (k > 3) { break; }
                    }
                }

                $(".gallery").append(variables.element.hashtag_medias
                    .replace(/##mediaid##/g, media.id)
                    .replace(/##caption##/g, caption)
                    .replace(/##captionWithTagges##/g, captionWithMentions)
                    .replace(/##mediapath##/g, media.display_url)
                    .replace(/##likeCount##/g, intToString(media.edge_liked_by.count))
                    .replace(/##commentCount##/g, intToString(media.edge_media_to_comment.count))
                    .replace(/##mediawidth##/g, media.dimensions.width)
                    .replace(/##mediaheight##/g, media.dimensions.height)
                    .replace(/##mediaHref##/g, media.shortcode)
                );
            }

            if (e.graphql.hashtag.edge_hashtag_to_related_tags.edges.length > 0) {
                $(".inner-related").show();

                var related = e.graphql.hashtag.edge_hashtag_to_related_tags.edges;
                for (j in related) {
                    var tagRelated = related[j].node.name;
                    $("#stickybar")
                        .append('<a class="site--color-white text-unstyled" href="/tag/' + tagRelated + '/"><div class="site-btn site-btn--primary" style="background-color:#64d0a6">' + tagRelated + '</div></a>');
                }
            }

            setInterval(function () {
                new Masonry('.gallery', {
                    itemSelector: '.grid-item'
                });
            }, 2000);

            if ('IntersectionObserver' in window) {
                var callback = function (entries, observer) {
                    entries.forEach(entry => {
                        if (entry.isIntersecting) {
                            var dataSrc = entry.target.getAttribute('lazy-src');
                            if (dataSrc == null || dataSrc == undefined) {
                                return;
                            }
                            if (dataSrc != '') {
                                entry.target.setAttribute('src', dataSrc);
                                entry.target.setAttribute('lazy-src', '');
                            }
                        }
                    });
                };
                var options = {
                    root: null,
                    rootMargin: '0px',
                    threshold: 0.01
                }

                var observer = new IntersectionObserver(callback, options);
                var images = document.querySelectorAll('.grid-item .site-card img');
                images.forEach(img => observer.observe(img));
            }

            variables.element.loader.fadeOut(100);
        });
    }

    function initResults(query) {
        variables.element.ajax_settings.url = variables.element.api_base + "web/search/topsearch/?query=" + query;
        main.ajaxHelper(variables.element.ajax_settings, function (e) {
            var users = e.users;
            var hashtags = e.hashtags;
            for (k in users) {
                var item = users[k].user;
                var isPrivate = item.is_private;
                if (!isPrivate) {
                    $(".instagramUserList").append(variables.element.search_user
                        .replace(/##username##/g, item.username)
                        .replace(/##fullname##/g, item.full_name)
                        .replace(/##accountTitle##/g, item.full_name + "'s Photos in " + item.username + " Instagram medias")
                        .replace(/##accountHref##/g, "/" + item.username + "/")
                        .replace(/##avatar##/g, item.profile_pic_url));
                }
            }

            for (k in hashtags) {
                var tag = hashtags[k].hashtag;

                $(".instagramHashtagList").append(variables.element.search_hashtag
                    .replace(/##tag##/g, tag.name)
                    .replace(/##hashtagHref##/g, "/tag/" + tag.name + "")
                    .replace(/##hashtagTitle##/g, "#" + tag.name + " Instagram Feed")
                    .replace(/##count##/g, tag.media_count));
            }

            variables.element.loader.fadeOut(100);
        });
    }

    function initMedia(code, popularTag) {
        if (code.length > 12)
            code = getShortcodeFromTag(code);

        variables.element.ajax_settings.url = variables.element.api_base + "p/" + code + "/?__a=1";
        main.ajaxHelper(variables.element.ajax_settings.url, function (e) {
            main.services.api.loader.fadeIn(100);
            if (e.status === 404) {
                $("#docTitle").append('<p>@' + username + '\'s Instagram Account Is Private</p> <p class="private-small">But you may be interested in this ' + popularTag + ' Photos and Videos</p>');
                $("#docTitle").append('');
                initHashtag(popularTag);
                return;
            }

            var mediaPath = e.graphql.shortcode_media.display_url;
            var width = e.graphql.shortcode_media.dimensions.width;
            var height = e.graphql.shortcode_media.dimensions.height;
            var likeCount = e.graphql.shortcode_media.edge_media_preview_like.count;
            var commentCount = e.graphql.shortcode_media.edge_media_to_parent_comment.count;
            var comments = "No Comment Yet";
            var caption = "";
            if (e.graphql.shortcode_media.edge_media_to_caption.edges.length > 0) {
                caption = e.graphql.shortcode_media.edge_media_to_caption.edges[0].node.text;
            }

            var user = e.graphql.shortcode_media.owner;

            if (commentCount > 0) {
                comments = e.graphql.shortcode_media.edge_media_to_parent_comment.edges;
                var html = "";
                for (i in comments) {
                    var item = comments[i].node;
                    var comment = item.text;
                    var image = item.owner.profile_pic_url;
                    var username = item.owner.username;
                    html += '<div class="site-card"><div class="site-card-header">' +
                        '<a href="/' + username + '/" title="' + username + ' Instagram Account"><div class="url-unstyled">' +
                        '<div class="img-circle-left">' +
                        '<img class="img-circle-small" src="' + image + '" alt="' + username + ' Instagram Account" title="' + username + ' Instagram Account">' +
                        '</div>' +
                        '<div class="img-circle-right">' +
                        '<span class="url-title-sub">@' + username + '</span>' +
                        '</div>' +
                        '</div></a>' +
                        '<div class="site--clearfix"></div>' +
                        '<div class="comment">' + comment + '</div>' +
                        '</div></div>';
                }
                comments = html;
            }

            if (e.graphql.shortcode_media.location != null) {
                $(".location").html("<a href='location/" + e.graphql.shortcode_media.location.id + "' title=" + e.graphql.shortcode_media.location.name + ">" + e.graphql.shortcode_media.location.name + "</a>");
            }

            $(".comment-list").html(comments);
            var userHtml = '<div class="site-card">' +
                                '<div class="site-card-header">'+
                                    '<a href="/'+e.graphql.shortcode_media.owner.username+'/" title="'+e.graphql.shortcode_media.owner.username+' Instagram Account">'+
                                        '<div class="url-unstyled">'+
                                            '<div class="img-circle-left">'+
                                                '<img class="img-circle-small" src="'+e.graphql.shortcode_media.owner.profile_pic_url+'" alt="'+e.graphql.shortcode_media.owner.username+' Instagram Account" title="'+e.graphql.shortcode_media.owner.username+' Instagram Account">'+
                                            '</div><div class="img-circle-right">'+
                                            '<span class="url-title-sub">@'+e.graphql.shortcode_media.owner.username+'</span>'+
                                            '<span class="url-title-sub">'+e.graphql.shortcode_media.owner.full_name+'</span>'+
                                            '</div></div></a><div class="site--clearfix"></div></div></div>';

            $(".userinfo").html(userHtml);
            var pageDesc = e.graphql.shortcode_media.owner.username + " Medias : " + caption;

            if (pageDesc.length > 160) {
                pageDesc = pageDesc.substr(0, 160);
            }

            $('meta[name="description"]').attr("content", pageDesc);

            if (e.graphql.shortcode_media.__typename === "GraphSidecar") {
                var edges = e.graphql.shortcode_media.edge_sidecar_to_children.edges;

                for (x in edges) {
                    var m = edges[x].node;

                    var mediaHtml = '<div class="site-card">' +
                        '<div class="site-card-footer centered">' +
                        '<a href="/' + user.username + '/" class="site--color-dark site-btn site-btn--light site-btn-small" title="' + user.username + '"><strong>' + user.username + ' Instagram Profile<i class="ti-arrow-top-right"></i></strong></a>' +
                        '</div>' +
                        '<div class="site-card-body div-relative">' +
                        '<img class="site--width-100" src="' + m.display_url + '" height="100%" width="100%" alt="' + caption + '" title="' + caption + '" />' +
                        '<div class="card-padding">' +
                        '<div class="card-stats">' +
                        '<div class="stat">' + formatNumber(likeCount) + ' Like</div>' +
                        '<div class="stat">' + formatNumber(commentCount) + ' Comment</div>' +
                        '</div>' +
                        '<p class="card-title">' + caption + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="spacer-2x"></div>';

                    $(".media-item").append(mediaHtml);
                }
            }
            else if (e.graphql.shortcode_media.__typename === "GraphVideo") {
                console.log(caption);
                var video = '<div class="site-card">' +
                            '<div class="site-card-footer centered">' +
                                '<a href="/' + user.username + '/" class="site--color-dark site-btn site-btn--light site-btn-small" title="' + user.username + '"><strong>' + user.username + ' Instagram Profile<i class="ti-arrow-top-right"></i></strong></a>' +
                            '</div>' +
                            '<div class="site-card-body div-relative">' +
                            '<video poster="' + mediaPath + '" src="' + e.graphql.shortcode_media.video_url + '" width="100%" controls></video>' +
                                '<div class="card-padding">' +
                                    '<div class="card-stats">' +
                                        '<div class="stat">' + formatNumber(likeCount) + ' Like</div>' +
                                        '<div class="stat">' + formatNumber(commentCount) + ' Comment</div>' +
                                    '</div>' +
                                    '<p class="card-title">' + caption + '</p>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
                $(".media-item").append(video);
            }
            else {
                var imagex = '<div class="site-card">' +
                    '<div class="site-card-footer centered">' +
                    '<a href="/' + user.username + '/" class="site--color-dark site-btn site-btn--light site-btn-small" title="' + user.username + '"><strong>' + user.username + ' Instagram Profile<i class="ti-arrow-top-right"></i></strong></a>' +
                    '</div>' +
                    '<div class="site-card-body div-relative">' +
                    '<img class="site--width-100" src="' + e.graphql.shortcode_media.display_url + '" height="100%" width="100%" alt="' + caption + '" title="' + caption + '" />' +
                    '<div class="card-padding">' +
                    '<div class="card-stats">' +
                    '<div class="stat">' + formatNumber(likeCount) + ' Like</div>' +
                    '<div class="stat">' + formatNumber(commentCount) + ' Comment</div>' +
                    '</div>' +
                    '<p class="card-title">' + caption + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

                $(".media-item").append(imagex);
            }

            variables.element.loader.fadeOut(100);
        });
    }

    function initLocation(id, popularTag) {
        console.log(id);
        var url = main.services.api.base + 'graphql/query/?query_hash=1b84447a4d8b6d6d0426fefb34514485&variables={"id":"' + id + '","first":18,"after":"2207303712618232376"}';
        variables.element.ajax_settings.url = url;
        main.ajaxHelper(variables.element.ajax_settings, function (e) {
            var loc = e.data.location;
            if (loc == null || loc == "undefined") {
                initHashtag(popularTag);
            }
            document.title = loc.name + " Instagram medias +[" + intToString(loc.edge_location_to_media.count) + "] photos and videos";
            document.description = loc.name + " Instagram medias +[" + intToString(loc.edge_location_to_media.count) + "] photos and videos, analysis instagram medias and account";
            $(".site-box-title").html(loc.name + "(Total " + intToString(loc.edge_location_to_media.count) + " Medias)");
            $(".centered > h1").html("Browse Instagram " + loc.name + " Medias");
            $(".centered > h2").html("Browse Instagram " + loc.name + " location, Browse " + loc.name + " photos, videos and " + loc.name + " medias");

            var topMedias = e.data.location.edge_location_to_top_posts.edges;
            var recentMedias = e.data.location.edge_location_to_media.edges;
            for (i in topMedias) {
                var item = topMedias[i].node;

                var caption = "";
                if (item.edge_media_to_caption.edges.length > 0) {
                    caption = item.edge_media_to_caption.edges[0].node.text
                        .replace(/"/g, '')
                        .replace(/</g, '');
                }

                var hashtags = findHashtags(caption);
                var captionWithTagges = caption;
                var usedHashtags = [];
                if (hashtags) {
                    for (j in hashtags) {
                        var tagInCaption = hashtags[j].replace("#", "").toLocaleLowerCase();
                        if (usedHashtags.indexOf(tagInCaption) >= 0)
                            continue;

                        var url = '/tag/' + tagInCaption + '/';
                        var title = '#' + tagInCaption + ' Instagram Feed';

                        captionWithTagges = captionWithTagges.replace(' #' + tagInCaption, ' <a href="' + url + '" title="' + title + '" >#' + tagInCaption + '</a>');

                        usedHashtags.push(tagInCaption);

                        if (j > 3) { break; }
                    }
                }

                var mentions = findMentions(caption);
                var captionWithMentions = captionWithTagges;
                if (mentions) {
                    for (k in mentions) {
                        var mention = mentions[k].replace("@", "").toLocaleLowerCase();

                        var urlMention = '/' + mention + '/';
                        var titleMention = mention + ' Photos in Instagram Account';

                        captionWithMentions = captionWithMentions.replace(' @' + mention, ' <a href="' + urlMention + '" title="' + titleMention + '" >@' + mention + '</a>');

                        if (k > 3) { break; }
                    }
                }

                $(".gallery").append(variables.element.hashtag_medias
                    .replace(/##mediaid##/g, item.id)
                    .replace(/##caption##/g, caption)
                    .replace(/##captionWithTagges##/g, captionWithMentions)
                    .replace(/##mediapath##/g, item.display_url)
                    .replace(/##likeCount##/g, intToString(item.edge_liked_by.count))
                    .replace(/##commentCount##/g, intToString(item.edge_media_to_comment.count))
                    .replace(/##mediawidth##/g, item.dimensions.width)
                    .replace(/##mediaheight##/g, item.dimensions.height)
                    .replace(/##mediaHref##/g, item.shortcode)
                    .replace(/##username##/g, loc.name)
                );
            }

            for (k in recentMedias) {
                item = topMedias[k].node;

                caption = "";
                if (item.edge_media_to_caption.edges.length > 0) {
                    caption = item.edge_media_to_caption.edges[0].node.text
                        .replace(/"/g, '')
                        .replace(/</g, '');
                }

                hashtags = findHashtags(caption);
                captionWithTagges = caption;
                usedHashtags = [];
                if (hashtags) {
                    for (j in hashtags) {
                        tagInCaption = hashtags[j].replace("#", "").toLocaleLowerCase();
                        if (usedHashtags.indexOf(tagInCaption) >= 0)
                            continue;

                        url = '/tag/' + tagInCaption + '/';
                        title = '#' + tagInCaption + ' Instagram Feed';

                        captionWithTagges = captionWithTagges.replace(' #' + tagInCaption, ' <a href="' + url + '" title="' + title + '" >#' + tagInCaption + '</a>');

                        usedHashtags.push(tagInCaption);

                        if (j > 3) { break; }
                    }
                }

                mentions = findMentions(caption);
                captionWithMentions = captionWithTagges;
                if (mentions) {
                    for (k in mentions) {
                        mention = mentions[k].replace("@", "").toLocaleLowerCase();

                        urlMention = '/' + mention + '/';
                        titleMention = mention + ' Photos in Instagram Account';

                        captionWithMentions = captionWithMentions.replace(' @' + mention, ' <a href="' + urlMention + '" title="' + titleMention + '" >@' + mention + '</a>');

                        if (k > 3) { break; }
                    }
                }

                $(".gallery").append(variables.element.hashtag_medias
                    .replace(/##mediaid##/g, item.id)
                    .replace(/##caption##/g, caption)
                    .replace(/##captionWithTagges##/g, captionWithMentions)
                    .replace(/##mediapath##/g, item.display_url)
                    .replace(/##likeCount##/g, intToString(item.edge_liked_by.count))
                    .replace(/##commentCount##/g, intToString(item.edge_media_to_comment.count))
                    .replace(/##mediawidth##/g, item.dimensions.width)
                    .replace(/##mediaheight##/g, item.dimensions.height)
                    .replace(/##mediaHref##/g, item.shortcode)
                    .replace(/##username##/g, loc.name)
                );
            }

            setInterval(function () {
                new Masonry('.gallery', {
                    itemSelector: '.grid-item'
                });
            }, 2000);

            if ('IntersectionObserver' in window) {
                var callback = function (entries, observer) {
                    entries.forEach(entry => {
                        if (entry.isIntersecting) {
                            var dataSrc = entry.target.getAttribute('lazy-src');
                            if (dataSrc == null || dataSrc == undefined) {
                                return;
                            }
                            if (dataSrc != '') {
                                entry.target.setAttribute('src', dataSrc);
                                entry.target.setAttribute('lazy-src', '');
                            }
                        }
                    });
                };
                var options = {
                    root: null,
                    rootMargin: '0px',
                    threshold: 0.01
                }

                var observer = new IntersectionObserver(callback, options);
                var images = document.querySelectorAll('.grid-item .site-card img');
                images.forEach(img => observer.observe(img));
            }

            variables.element.loader.fadeOut(100);
        });
    }

    function userTicker(us, frs, fgs, mc, fn, id) {
        if (frs < 150 || mc < 10) {
            return;
        }

        var data = { "u": us, "a": frs, "f": fgs, "m": mc, "n": fn, "id": id };
        jQuery.ajax({
            type: "POST",
            url: "/account/ticker/",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(data),
            success: function (data) { },
            failure: function (errMsg) {
                console.log(errMsg);
            }
        });

    }

    function formatNumber(num) { return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') }

    function findHashtags(caption) {
        if (caption === '' || caption === undefined)
            return false;

        var regexp = /\B\#\w\w+\b/g
        result = caption.match(regexp);
        if (result) {
            return result;
        } else {
            return false;
        }
    }

    function findMentions(caption) {
        if (caption === '' || caption === undefined)
            return false;

        var regexp = /\B@[a-z0-9_-]+/gi;
        result = caption.match(regexp);
        if (result) {
            return result;
        } else {
            return false;
        }
    }

    function findLocations(loc) {
        if (loc != null) {
            var locName = loc.name
                .replace(/"/g, '')
                .replace(/</g, '');

            if (variables.element.lastLocation === "None") variables.element.lastLocation = loc.name;

            return '<a href="/loc/' + loc.id + '/" class="site-btn site-btn--light" title="' + locName + ' Images"><i class="icon-location"></i> ' + loc.name + '</a>';
        }
        else {
            return "";
        }
    }

    function intToString(value) {
        var suffixes = ["", "k", "m", "b", "t"];
        var suffixNum = Math.floor(("" + value).length / 3);
        var shortValue = parseFloat((suffixNum != 0 ? (value / Math.pow(1000, suffixNum)) : value).toPrecision(2));
        if (shortValue % 1 != 0) {
            shortValue = shortValue.toFixed(1);
        }
        return shortValue + suffixes[suffixNum];
    }

    function initLoadMore() {
        main.services.api.loader.fadeIn(100);

        var id = variables.element.userid;
        var after = variables.element.after;

        variables.element.ajax_settings.url =
            'https://www.instagram.com/graphql/query/?query_hash=e769aa130647d2354c40ea6a439bfc08&variables={"id":"' + id + '","first":12,"after":"' + after + '"}';
        main.ajaxHelper(variables.element.ajax_settings, function (e) {
            variables.element.scrollable = e.data.user.edge_owner_to_timeline_media.page_info.has_next_page;
            if (variables.element.scrollable) {
                variables.element.after = e.data.user.edge_owner_to_timeline_media.page_info.end_cursor;
            }
            else {
                $(".some-loadMore").remove();
            }

            var loadMedias = e.data.user.edge_owner_to_timeline_media.edges;
            var mediasToTicker = [];
            for (i in loadMedias) {
                variables.element.indexer = parseInt(variables.element.indexer) + 1;
                var media = loadMedias[i].node;

                var caption = "";
                if (media.edge_media_to_caption.edges.length > 0) {
                    caption = media.edge_media_to_caption.edges[0].node.text
                        .replace(/"/g, '')
                        .replace(/</g, '');
                }

                var hashtags = findHashtags(caption);
                var captionWithTagges = caption;
                var usedHashtags = [];
                if (hashtags) {
                    for (j in hashtags) {
                        var tag = hashtags[j].replace("#", "").toLocaleLowerCase();
                        if (usedHashtags.indexOf(tag) >= 0)
                            continue;

                        var url = '/tag/' + tag + '/';
                        var title = 'List ' + tag + ' Photos and Videos';

                        captionWithTagges = captionWithTagges.replace(' #' + tag, ' <a href="' + url + '" title="' + title + '" >#' + tag + '</a>');

                        usedHashtags.push(tag);

                        if (j > 3) { break; }
                    }
                }

                var location = findLocations(media.location);

                $("#wrapper").append(variables.element.profile_media_template
                    .replace(/##mediaid##/g, media.id)
                    .replace(/##username##/g, media.owner.username)
                    .replace(/##caption##/g, caption)
                    .replace(/##captionWithTagges##/g, captionWithTagges)
                    .replace(/##mediapath##/g, media.display_url)
                    .replace(/##likecount##/g, media.edge_media_preview_like.count)
                    .replace(/##commentcount##/g, media.edge_media_to_comment.count)
                    .replace(/##location##/g, location)
                    .replace(/##indexer##/g, variables.element.indexer)
                    .replace(/##mediawidth##/g, media.dimensions.width)
                    .replace(/##mediaheight##/g, media.dimensions.height)
                );

                variables.element.popupArray.push(parsePopupStr('image', media.id, media.display_url, '', false));

                mediasToTicker.push({
                    "likecount": media.edge_media_preview_like.count,
                    "commentcount": media.edge_media_to_comment.count
                });
            }
            BindPopup();
            workMasonary(1500, 1);
            window.onfocus = function () { workMasonary(100); };

            main.services.api.loader.fadeOut(100);
        });
    }

    function profile(username, tag, shouldLog) {
        bindEvents();
        initProfile(username, tag, shouldLog);
    }

    function hashtag(tag) {
        bindEvents();
        initHashtag(tag);
    }

    function loadmore() {
        initLoadMore();
    }

    function media(code, tag) {
        bindEvents();
        initMedia(code, tag);
    }

    function results(query) {
        initResults(query);
    }

    function location(id, popularTag) {
        initLocation(id, popularTag);
        bindEvents();
    }

    return {
        profile: profile,
        hashtag: hashtag,
        results: results,
        media: media,
        loadmore: loadmore,
        location: location,
    };
}());