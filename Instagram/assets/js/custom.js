
function divChecker() {
    ! function (t, e) {
        "function" == typeof define && define.amd ? define(e) : "object" == typeof module && module.exports ? module.exports = e() : t.Colcade = e()
    }(window, function () {
        function t(t, e) {
            if ((t = r(t)) && t.colcadeGUID) {
                var i = n[t.colcadeGUID];
                return i.option(e), i
            }
            this.element = t, this.options = {}, this.option(e), this.create()
        }
        var e = t.prototype;
        e.option = function (t) {
            this.options = function (t, e) {
                for (var i in e) t[i] = e[i];
                return t
            }(this.options, t)
        };
        var i = 0,
            n = {};

        function o(e) {
            var i = {};
            e.getAttribute("data-colcade").split(",").forEach(function (t) {
                var e = t.split(":"),
                    n = e[0].trim(),
                    o = e[1].trim();
                i[n] = o
            }), new t(e, i)
        }

        function s(t) {
            var e = [];
            if (Array.isArray(t)) e = t;
            else if (t && "number" == typeof t.length)
                for (var i = 0; i < t.length; i++) e.push(t[i]);
            else e.push(t);
            return e
        }

        function u(t, e) {
            return s((e = e || document).querySelectorAll(t))
        }

        function r(t) {
            return "string" == typeof t && (t = document.querySelector(t)), t
        }
        return e.create = function () {
            this.errorCheck();
            var t = this.guid = ++i;
            this.element.colcadeGUID = t, n[t] = this, this.reload(), this._windowResizeHandler = this.onWindowResize.bind(this), this._loadHandler = this.onLoad.bind(this), window.addEventListener("resize", this._windowResizeHandler), this.element.addEventListener("load", this._loadHandler, !0)
        }, e.errorCheck = function () {
            var t = [];
            if (this.element || t.push("Bad element: " + this.element), this.options.columns || t.push("columns option required: " + this.options.columns), this.options.items || t.push("items option required: " + this.options.items), t.length) throw new Error("[Colcade error] " + t.join(". "))
        }, e.reload = function () {
            this.updateColumns(), this.updateItems(), this.layout()
        }, e.updateColumns = function () {
            this.columns = u(this.options.columns, this.element)
        }, e.updateItems = function () {
            this.items = u(this.options.items, this.element)
        }, e.getActiveColumns = function () {
            return this.columns.filter(function (t) {
                return "none" != getComputedStyle(t).display
            })
        }, e.layout = function () {
            this.activeColumns = this.getActiveColumns(), this._layout()
        }, e._layout = function () {
            this.columnHeights = this.activeColumns.map(function () {
                return 0
            }), this.layoutItems(this.items)
        }, e.layoutItems = function (t) {
            t.forEach(this.layoutItem, this)
        }, e.layoutItem = function (t) {
            var e = Math.min.apply(Math, this.columnHeights),
                i = this.columnHeights.indexOf(e);
            this.activeColumns[i].appendChild(t), this.columnHeights[i] += t.offsetHeight || 1
        }, e.append = function (t) {
            var e = this.getQueryItems(t);
            this.items = this.items.concat(e), this.layoutItems(e)
        }, e.prepend = function (t) {
            var e = this.getQueryItems(t);
            this.items = e.concat(this.items), this._layout()
        }, e.getQueryItems = function (t) {
            t = s(t);
            var e = document.createDocumentFragment();
            return t.forEach(function (t) {
                e.appendChild(t)
            }), u(this.options.items, e)
        }, e.measureColumnHeight = function (t) {
            var e = this.element.getBoundingClientRect();
            this.activeColumns.forEach(function (i, n) {
                if (!t || i.contains(t)) {
                    var o = i.lastElementChild.getBoundingClientRect();
                    this.columnHeights[n] = o.bottom - e.top
                }
            }, this)
        }, e.onWindowResize = function () {
            clearTimeout(this.resizeTimeout), this.resizeTimeout = setTimeout(function () {
                this.onDebouncedResize()
            }.bind(this), 100)
        }, e.onDebouncedResize = function () {
            var t = this.getActiveColumns(),
                e = t.length == this.activeColumns.length,
                i = !0;
            this.activeColumns.forEach(function (e, n) {
                i = i && e == t[n]
            }), e && i || (this.activeColumns = t, this._layout())
        }, e.onLoad = function (t) {
            this.measureColumnHeight(t.target)
        }, e.destroy = function () {
            this.items.forEach(function (t) {
                this.element.appendChild(t)
            }, this), window.removeEventListener("resize", this._windowResizeHandler), this.element.removeEventListener("load", this._loadHandler, !0), delete this.element.colcadeGUID, delete n[this.guid]
        },
            function (t) {
                if ("complete" == document.readyState) return void t();
                document.addEventListener("DOMContentLoaded", t)
            }(function () {
                u("[data-colcade]").forEach(o)
            }), t.data = function (t) {
                var e = (t = r(t)) && t.colcadeGUID;
                return e && n[e]
            }, t.makeJQueryPlugin = function (e) {
                (e = e || window.jQuery) && (e.fn.colcade = function (i) {
                    if ("string" == typeof i) {
                        var n = Array.prototype.slice.call(arguments, 1);
                        return s = i, u = n, (o = this).each(function (t, i) {
                            var n = e.data(i, "colcade");
                            if (n) {
                                var o = n[s].apply(n, u);
                                r = void 0 === r ? o : r
                            }
                        }), void 0 !== r ? r : o
                    }
                    var o, s, u, r, a;
                    return a = i, this.each(function (i, n) {
                        var o = e.data(n, "colcade");
                        o ? (o.option(a), o.layout()) : (o = new t(n, a), e.data(n, "colcade", o))
                    }), this
                })
            }, t.makeJQueryPlugin(), t
    });
}

    

