﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using Instagram.Data;
using Instagram.Model;

namespace Instagram.Helpers
{
    public class CacheHelper
    {
        private static string Categories_CacheKey = "Categories_CacheKey";
        private static string Settings_CacheKey = "Settings_CacheKey";
        private static string BanUsers_CacheKey = "BanUsers_CacheKey";
        private static string BanLocations_CacheKey = "BanLocations_CacheKey";
        private static string BanHashtags_CacheKey = "BanHashtags_CacheKey";
        private static string BanMedias_CacheKey = "BanMedias_CacheKey";
        private static string Popular_CacheKey = "Popular_CacheKey";
        private static string Hashtags_CacheKey = "Hashtags_CacheKey";

        public static List<Category> Categories
        {
            get
            {
                if (HttpContext.Current.Cache[Categories_CacheKey] == null)
                {
                    var categories = new CategoryData().GetAll();

                    HttpContext.Current.Cache.Add(
                          Categories_CacheKey, categories
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[Categories_CacheKey] as List<Category>;
            }
        }

        public static List<PopularAccount> Populars
        {
            get
            {
                if (HttpContext.Current.Cache[Popular_CacheKey] == null)
                {
                    var categories = new PopularAccountData().GetRandom(250);

                    HttpContext.Current.Cache.Add(
                          Popular_CacheKey, categories
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[Popular_CacheKey] as List<PopularAccount>;
            }
        }

        public static List<Hashtag> Hashtags
        {
            get
            {
                if (HttpContext.Current.Cache[Hashtags_CacheKey] == null)
                {
                    var categories = new HashtagData().GetRandom(250);

                    HttpContext.Current.Cache.Add(
                          Hashtags_CacheKey, categories
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[Hashtags_CacheKey] as List<Hashtag>;
            }
        }

        public static Setting Setting
        {
            get
            {
                if (HttpContext.Current.Cache[Settings_CacheKey] == null)
                {
                    string siteId = ConfigurationManager.AppSettings["Instagram.SiteId"];
                    int sit = Int32.Parse(siteId);
                    var setting = new SettingData().GetByKey(sit);

                    HttpContext.Current.Cache.Add(
                          Settings_CacheKey, setting
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[Settings_CacheKey] as Setting;
            }
        }

        public static List<BanUser> BanUsers
        {
            get
            {
                if (HttpContext.Current.Cache[BanUsers_CacheKey] == null)
                {
                    var banwords = new BanUserData().GetAll();

                    HttpContext.Current.Cache.Add(
                          BanUsers_CacheKey, banwords
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[BanUsers_CacheKey] as List<BanUser>;
            }
        }

        public static List<BanLocation> BanLocations
        {
            get
            {
                if (HttpContext.Current.Cache[BanLocations_CacheKey] == null)
                {
                    var banwords = new BanLocationData().GetAll();

                    HttpContext.Current.Cache.Add(
                          BanLocations_CacheKey, banwords
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[BanLocations_CacheKey] as List<BanLocation>;
            }
        }
        
        public static List<BanHashtag> BanHashtags
        {
            get
            {
                if (HttpContext.Current.Cache[BanHashtags_CacheKey] == null)
                {
                    var banwords = new BanHashtagData().GetAll();

                    HttpContext.Current.Cache.Add(
                          BanHashtags_CacheKey, banwords
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[BanHashtags_CacheKey] as List<BanHashtag>;
            }
        }

        public static List<BanMedia> BanMedias
        {
            get
            {
                if (HttpContext.Current.Cache[BanMedias_CacheKey] == null)
                {
                    var banwords = new BanMediaData().GetAll();

                    HttpContext.Current.Cache.Add(
                          BanMedias_CacheKey, banwords
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[BanMedias_CacheKey] as List<BanMedia>;
            }
        }

        public static void ClearCaches()
        {
            HttpContext.Current.Cache.Remove("Categories_CacheKey");
            HttpContext.Current.Cache.Remove("Settings_CacheKey");
        }

        public static void ClearBans()
        {
            HttpContext.Current.Cache.Remove("BanUsers_CacheKey");
            HttpContext.Current.Cache.Remove("BanLocations_CacheKey");
            HttpContext.Current.Cache.Remove("BanHashtags_CacheKey");
            HttpContext.Current.Cache.Remove("BanMedias_CacheKey");
        }
    }
}