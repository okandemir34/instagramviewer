﻿namespace Instagram.Data
{
    using Instagram.Data.Core;

    public class BanHashtagData : Core.EntityBaseData<Model.BanHashtag>
    {
        public BanHashtagData() : base(new DataContext()) { }
    }
}
