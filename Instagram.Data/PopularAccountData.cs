﻿namespace Instagram.Data
{
    using Instagram.Data.Core;

    public class PopularAccountData : Core.EntityBaseData<Model.PopularAccount>
    {
        public PopularAccountData() : base(new DataContext()) { }
    }
}
