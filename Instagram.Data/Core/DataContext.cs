﻿namespace Instagram.Data.Core
{
    using Instagram.Model;
    using System.Data.Entity;

    public class DataContext : DbContext
    {
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Hashtag> Hashtags { get; set; }
        public DbSet<PopularAccount> PopularAccounts { get; set; }
        public DbSet<Remove> Removes { get; set; }
        public DbSet<BanUser> BanUsers { get; set; }
        public DbSet<BanHashtag> BanHashtags { get; set; }
        public DbSet<BanLocation> BanLocations { get; set; }
        public DbSet<BanMedia> BanMedias { get; set; }
        public DbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Setting>().ToTable("ins_Settings");
            modelBuilder.Entity<Category>().ToTable("ins_Categories");
            modelBuilder.Entity<Hashtag>().ToTable("ins_Hashtags");
            modelBuilder.Entity<PopularAccount>().ToTable("ins_PopularAccounts");
            modelBuilder.Entity<Remove>().ToTable("ins_Removes");
            modelBuilder.Entity<BanUser>().ToTable("ins_BanUsers");
            modelBuilder.Entity<BanHashtag>().ToTable("ins_BanHashtags");
            modelBuilder.Entity<BanLocation>().ToTable("ins_BanLocations");
            modelBuilder.Entity<BanMedia>().ToTable("ins_BanMedias");
            modelBuilder.Entity<Log>().ToTable("ins_Logs");

            base.OnModelCreating(modelBuilder);
        }
    }
}