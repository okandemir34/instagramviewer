﻿namespace Instagram.Data
{
    using Instagram.Data.Core;

    public class RemoveData : Core.EntityBaseData<Model.Remove>
    {
        public RemoveData() : base(new DataContext()) { }
    }
}
