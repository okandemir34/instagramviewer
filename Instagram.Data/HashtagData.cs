﻿namespace Instagram.Data
{
    using Instagram.Data.Core;

    public class HashtagData : Core.EntityBaseData<Model.Hashtag>
    {
        public HashtagData() : base(new DataContext()) { }
    }
}
