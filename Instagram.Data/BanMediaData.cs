﻿namespace Instagram.Data
{
    using Instagram.Data.Core;

    public class BanMediaData : Core.EntityBaseData<Model.BanMedia>
    {
        public BanMediaData() : base(new DataContext()) { }
    }
}
