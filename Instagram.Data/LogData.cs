﻿namespace Instagram.Data
{
    using Instagram.Data.Core;

    public class LogData : Core.EntityBaseData<Model.Log>
    {
        public LogData() : base(new DataContext()) { }
    }
}
