﻿namespace Instagram.Data
{
    using Instagram.Data.Core;

    public class BanUserData : Core.EntityBaseData<Model.BanUser>
    {
        public BanUserData() : base(new DataContext()) { }
    }
}
