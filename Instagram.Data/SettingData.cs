﻿namespace Instagram.Data
{
    using Instagram.Data.Core;

    public class SettingData : Core.EntityBaseData<Model.Setting>
    {
        public SettingData() : base(new DataContext()) { }
    }
}
