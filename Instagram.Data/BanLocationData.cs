﻿namespace Instagram.Data
{
    using Instagram.Data.Core;

    public class BanLocationData : Core.EntityBaseData<Model.BanLocation>
    {
        public BanLocationData() : base(new DataContext()) { }
    }
}
