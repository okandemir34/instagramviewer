﻿namespace Instagram.Data
{
    using Instagram.Data.Core;

    public class CategoryData : Core.EntityBaseData<Model.Category>
    {
        public CategoryData() : base(new DataContext()) { }
    }
}
